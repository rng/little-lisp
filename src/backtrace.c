#include "backtrace.h"
#include "common.h"
#include "dict.h"
#include <stdio.h>

obj_t *trace_expr_for_pc(obj_t *trace, uint8_t pc)
{
  for (int i=1; i<trace->size; i++) {
    obj_t *entry = list_val(trace, i);
    int trace_pc = NUM_VALUE(list_val(entry, 0));
    if (trace_pc > pc) {
      return list_val(entry, 1);
    }
  }
  if (trace->size > 1) {
    return list_val(list_val(trace, trace->size-1), 1);
  }
  return NULL;
}

void backtrace(const char *mode, obj_t *trace, obj_t *exc_obj, comp_state_t *compstate)
{
    if (obj_is_type(trace, OBJ_TYPE_BYTEARRAY)) {
      bytearray_t *bt = (bytearray_t*)trace;
      common_error_t err = bt->data[0];
      printf("%s exception:\n", mode);
      for (int i=2; i<bt->bytesize; i+=3) {
        uint16_t fnid = (bt->data[i]<<8) | (bt->data[i+1]);
        obj_t *func = NULL;
        bool ok = dict_get(compstate->debugmap, NUM_NEW(fnid), &func);
        uint8_t pc = bt->data[i+2];
        printf(" in ");
        if (ok) {
          obj_t *funcname = list_val(func, 0);
          obj_t *fexpr = trace_expr_for_pc(func, pc);
          printf("%s: %s", string_value(obj_to_string(funcname)),
                           fexpr ? string_value(obj_to_string(fexpr)) : "?");
        } else {
          printf("?");
        }
        printf("\n");
      }
      printf("%s ", common_error_str(err));
      if (exc_obj) {
        printf("%s", string_value(obj_to_string(exc_obj)));
      }
      printf("\n");
    } else {
      printf("bad backtrace\n");
    }
}


