#include <assert.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include "comp.h"
#include "dict.h"
#include "parse.h"
#include "macro.h"
#include "intern.h"
#include "remote.h"
#include "socket.h"
#include "backtrace.h"
#include "lvm.h"

#include <readline/readline.h>
#include <readline/history.h>

#define HEAP_WORD_SIZE 32*1024

typedef struct {
  uint32_t heap[HEAP_WORD_SIZE];
  macros_t macros;
  comp_state_t compstate;
  pid_t vm;
  bool quit;
} llisp_t;

llisp_t llisp;

bool do_file(llisp_t *llisp, char *fn);

char *load_file(char *fn)
{
  FILE *f = fopen(fn, "rb");
  if (!f) {
    FATAL("File not found: %s", fn);
  }
  fseek(f, 0, SEEK_END);
  size_t sz = ftell(f);
  rewind(f);
  char *data = (char*)malloc(sz+6);
  strcpy(data, "(do ");
  size_t read = fread(data+4, 1, sz, f);
  assert(read == sz);
  data[sz+4]=')';
  data[sz+5]=0;
  return data; 
}

obj_t *run(llisp_t *llisp, obj_t *code, bool *ok)
{
  obj_t *r = NULL;
  GC_ENTER2(code, r);
  *ok = remote_run((bytearray_t*)code, &r);
  GC_EXIT(r);
}

bool func_is_special(obj_t *sym)
{
  uint32_t v = symbol_value(sym);
  return ((v == intern("exit")) ||
          (v == intern("load")));
}

bool eval_special(llisp_t *llisp, obj_t *expr, obj_t **ret)
{
  *ret = NULL;
  GC_ENTER2(expr, *ret);
  uint32_t argcount = list_size(expr) - 1;
  uint32_t fn = symbol_value(list_val(expr, 0));
  if ((fn == intern("exit")) && (argcount == 0)) {
    llisp->quit = true;
    GC_EXIT(true);
  } else
  if ((fn == intern("load")) && (argcount == 1)) {
    *ret = NUM_NEW(do_file(llisp, string_value(list_val(expr, 1))));
    GC_EXIT(true);
  }
  common_error1(ERROR_VM_THROW, NULL);
  GC_EXIT(false);
}

bool eval(llisp_t *llisp, obj_t *expr, obj_t **r)
{
  obj_t *code = NULL;
  bool isdef = false;
  bool ismacro = false;
  bool isspecial = false;
  bool ok = true;
  gc_stack_t *exception;

  GC_ENTER3(expr, code, *r);
  TRY(exception) {
    // compile
    code = comp_compile(&llisp->compstate, expr, &llisp->macros, &isdef);
    // eval in macro env if needed
    if (obj_is_type(expr, OBJ_TYPE_LIST) && 
        list_size(expr) &&
        obj_is_type(list_val(expr, 0), OBJ_TYPE_SYMBOL)) {
      ismacro = symbol_value(list_val(expr, 0)) == intern("defmacro");
      isspecial = func_is_special(list_val(expr, 0));
      if (ismacro || isdef) {
        macro_eval(&llisp->macros, code);
      }
      if (isspecial) {
        ok = eval_special(llisp, expr, r);
      }
    }

    // eval on target
    if (!(ismacro || isspecial)) {
      *r = run(llisp, code, &ok);
      if (!ok) {
        backtrace("Runtime", *r, remote_get_exception(), &llisp->compstate);
        *r = NULL;
      }
    }
  } else {
    EXCEPT_ENTER(exception);
    obj_t *trace = vm_backtrace(&llisp->macros.vm, _exception_err);
    backtrace("Compiler", trace, _exception_obj, &llisp->compstate);
    ok = false;
  }
  GC_EXIT(ok);
}

bool parse(char *data, obj_t **r)
{
  bool ok = true;
  gc_stack_t *exception;
  GC_ENTER1(*r);
  TRY(exception) {
    *r = parse_parse(data);
  } else {
    EXCEPT_ENTER(exception);
    printf("parse exception: %s\n", common_error_str(_exception_err));
    *r = NULL;
    ok = false;
  }
  GC_EXIT(ok);
}

bool do_file(llisp_t *llisp, char *fn)
{
  obj_t *r = NULL;
  obj_t *tmp = NULL;

  GC_ENTER2(r, tmp);
  char *data = load_file(fn);
  parse(data, &r);
  free(data);
  for (int i=0; i<list_size(r)-1; i++) {
    if (!eval(llisp, list_val(r, i+1), &tmp)) {
      GC_EXIT(false);
    }
  }
  GC_EXIT(true);
}

void repl(llisp_t *llisp)
{
  obj_t *expr = NULL;
  obj_t *r = NULL;
  GC_ENTER2(expr, r);
  char *data = readline("> ");
  if (data && strlen(data)) {
    add_history(data);
    if (parse(data, &expr)) {
      if (eval(llisp, expr, &r)) {
        printf("%s\n", string_value(obj_to_string(r)));
      }
    }
  } else if (!data) {
    llisp->quit = true;
  }
  free(data);
  GC_EXIT0();
}

void llisp_close(int n)
{
  kill(llisp.vm, SIGHUP);
  write_history(".replhist");
  exit(0);
}

void llisp_break(int n)
{
  remote_break();
}

pid_t fork_vm(void)
{
  pid_t pid = fork();
  if (pid < 0) {
    return pid;
  } else if (pid == 0) {
    lvm_main();
  }
  return pid;
}

void llisp_init(llisp_t *llisp)
{
  memset(llisp, 0, sizeof(llisp_t));

  // launch vm
  llisp->vm = fork_vm();
  usleep(10000);

  // init
  mem_init(llisp->heap, HEAP_WORD_SIZE);
  macro_init(&llisp->macros, &llisp->compstate);
  llisp->compstate.debugmap = dict_new(48);
  remote_reset();
}

int main(int argc, char *argv[])
{
  llisp_init(&llisp);
  GC_ENTER3(llisp.macros.vm.env, 
            llisp.compstate.debugmap,
            _exception_obj);

  if (!do_file(&llisp, "lib/lib.lisp")) {
    FATAL("failed to load bootstrap");
  }
  for (int i=1; i<argc; i++) {
      if (!do_file(&llisp, argv[i])) {
        FATAL("failed to load script");
      }
  }

  signal(SIGINT, llisp_break);
  read_history(".replhist");
  while (!llisp.quit) {
      repl(&llisp);
  }
  llisp_close(0);
  GC_EXIT(0);
}
