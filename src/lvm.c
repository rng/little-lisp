#include <string.h>
#include "mem.h"
#include "cmd.h"
#include "vm.h"

#define VM_HEAP_WORD_SIZE 4*1024

void lvm_main(void)
{
  uint32_t heap[VM_HEAP_WORD_SIZE];
  vm_t vm;
  memset(&vm, 0, sizeof(vm));
  
  mem_init(heap, VM_HEAP_WORD_SIZE);
  vm_init(&vm);
  
  GC_ENTER5(vm.env, vm.code, vm.ds.buf, vm.cs.buf, _exception_obj);
  cmd_mainloop(&vm);
  GC_EXIT0();
}
