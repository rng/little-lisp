#include "remote.h"
#include "socket.h"
#include "wire.h"
#include <stdio.h>

bool remote_should_break;

obj_t *remote_do(wire_cmd_t cmd, obj_t *obj, wire_cmd_t *rcmd)
{
  bool ok;
  uint8_t buf[1024];
  size_t buf_len = sizeof(buf_len);
  obj_t *ret = NULL;

  GC_ENTER2(ret, obj);

  int client = client_init();
  ok = wire_encode(buf, &buf_len, cmd, obj);
  assert(ok);
  client_send(client, buf, buf_len);

  bool done = false;
  while (!done) {
    size_t recv_len = client_recv(client, buf, sizeof(buf));
    if (recv_len) {
      uint8_t *bufptr = buf;
      while (recv_len > 0) {
        ssize_t decode_len = wire_decode(bufptr, recv_len, rcmd, &ret);
        assert(decode_len >= 0);
        switch (*rcmd) {
          case WIRE_CMD_PRINT:
            printf("%s", string_value(obj_to_string_raw(ret)));
            break;
          case WIRE_CMD_CONT:
            break;
          default:
            // ACK, NACK, STOPPED, FAILED
            done = true;
            break;
        }
        bufptr += decode_len;
        recv_len -= decode_len;
      }
    }

    if (remote_should_break) {
      remote_stop();
      remote_should_break = false;
    }
  }
  client_shutdown(client);
  GC_EXIT(ret);
}

bool remote_run(bytearray_t *code, obj_t **ret)
{
  wire_cmd_t rcmd;
  GC_ENTER2(code, *ret);
  *ret = remote_do(WIRE_CMD_RUN, (obj_t*)code, &rcmd);
  GC_EXIT(rcmd == WIRE_CMD_STOPPED);
}

void remote_stop(void)
{
  wire_cmd_t rcmd;
  remote_do(WIRE_CMD_STOP, NULL, &rcmd);
}

obj_t *remote_get_exception(void)
{
  wire_cmd_t rcmd;
  return remote_do(WIRE_CMD_GET_EXCEPTION_PARAM, NULL, &rcmd);
}

void remote_reset(void)
{
  wire_cmd_t rcmd;
  remote_do(WIRE_CMD_RESET, NULL, &rcmd);
}

void remote_break(void)
{
  remote_should_break = true;
}

