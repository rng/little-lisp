#include "env.h"
#include "intern.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

env_t *env_new()
{
  env_t *e = malloc(sizeof(env_t));
  memset(e, 0, sizeof(env_t));
  return e;
}

void env_free(env_t *env)
{
}

bool env_get(env_t *env, obj_t *sym, int *d, int *n)
{
  assert(env);
  *d = 0;
  *n = 0;
  while (env) {
    for (int i=0; i<env->num_vars; i++) {
      if (sym == env->vars[i]) {
        *n = i;
        return true;
      }
    }
    env = env->parent;
    *d = *d + 1;
  }
  return false;
}

env_t *env_extend(env_t *parent, obj_t *args, int *argcount, bool *vararg)
{
  env_t *env = env_new();
  env->parent = parent;
  env->vars = (obj_t**)malloc(sizeof(obj_t*)*list_size(args));

  obj_t *amp = symbol_new(intern("&"));
  *vararg = false;

  int argc = 0;
  for (int i=0; i<list_size(args); i++) {
    obj_t *arg = list_val(args, i);
    assert(obj_is_type(arg, OBJ_TYPE_SYMBOL));
    if (arg == amp) {
      *vararg = true;
    } else {
      env->vars[argc++] = arg;
    }
  }
  env->num_vars = argc;
  *argcount = argc;
  return env;
}

