#include "codegen.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

void cg_new(cg_t *cg)
{
  cg->code_max = 256;
  cg->code_len = 0;
  cg->code = malloc(cg->code_max);
}

void cg_byte(cg_t *cg, uint8_t b)
{
  assert(cg->code_len < cg->code_max);
  cg->code[cg->code_len++] = b;
}

void cg_bytes(cg_t *cg, uint8_t *b, size_t len)
{
  for (int i=0; i<len; i++) {
    cg_byte(cg, b[i]);
  }
}

void cg_inst0(cg_t *cg, vm_op_t op, uint8_t n)
{
  assert(n <= 15);
  cg_byte(cg, (op<<4)|n);
}

void cg_insts(cg_t *cg, vm_op_t op, uint8_t l)
{
  if (l < 15) {
    cg_byte(cg, (op<<4)|l);
  } else {
    assert(l <= 255);
    cg_byte(cg, (op<<4)|15);
    cg_byte(cg, l);
  }
}

void cg_instp(cg_t *cg, vm_op_t op, uint8_t n, uint8_t p)
{
  assert(n <= 15);
  cg_byte(cg, (op<<4)|n);
  cg_byte(cg, p);
}

void cg_insti(cg_t *cg, vm_op_t op, int32_t v)
{
  if ((-7 <= v) && (v <= 7)) {
    int n = v+7;
    cg_byte(cg, (op<<4)|n);
  } else {
    assert(v <= 255);
    assert(v > 0);
    cg_byte(cg, (op<<4)|15);
    cg_byte(cg, v);
  }
}

int cg_instj(cg_t *cg, vm_op_t op, uint8_t n)
{
  assert(n <= 15);
  cg_byte(cg, (op<<4)|n);
  cg_byte(cg, 0);
  return cg->code_len - 2;
}

void cg_patch(cg_t *cg, int pos)
{
  uint8_t v = cg->code[pos];
  vm_op_t op = (v>>4);
  assert((op==VM_OP_JZ) || (op==VM_OP_JMP) ||
         (op==VM_OP_FN) || (op==VM_OP_FNV) ||
         (op==VM_OP_LIT));
  int j = cg->code_len - pos - 1;
  assert(j > 0);
  assert(j <= 255);
  cg->code[pos+1] = j;
}

void cg_funcid(cg_t *cg, int fnid)
{
  assert(fnid < 65535);
  cg_byte(cg, fnid>>8);
  cg_byte(cg, fnid&0xFF);
}

