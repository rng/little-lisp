#include "comp.h"
#include "ops.h"
#include "common.h"
#include "codegen.h"
#include "intern.h"
#include "macro.h"
#include "env.h"
#include "ffi.h"
#include "dict.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

typedef struct {
  bool isdef;
  macros_t *macros;
  cg_t cg;
  obj_t *lits;
  uint16_t latest_fnid;
  obj_t *latest_def;
  comp_state_t *state;
} comp_t;

void comp_expr(comp_t *comp, obj_t *expr, env_t *env, bool tail);

obj_t *lit_add_use(obj_t *lits, obj_t *lit, uint16_t use)
{
  obj_t *uses = NULL;
  GC_ENTER3(lits, lit, uses);

  if (lits == NULL) {
      lits = dict_new(16);
  }
  assert(obj_is_type(lits, OBJ_TYPE_DICT));
  assert(lit);
  if (dict_get(lits, lit, &uses)) {
      uses = list_cons(uses, NUM_NEW(use));
  } else {
      uses = list_new(1);
      list_set_val(uses, 0, NUM_NEW(use));
  }
  dict_set(&lits, lit, uses);

  GC_EXIT(lits);
}

static const char *prim_names[] = {
  "=",">","<","%/","%*","%-","%+","print","list","%cons","%first","%rest","count",
  "nth","throw","list?","append","aref","length","string-append","not","null?","make-hash",
  "hash-set!","hash-ref","%ffi-lookup-sym","ffi-call",NULL};

int prim_num(obj_t *sym)
{
  if (!obj_is_type(sym, OBJ_TYPE_SYMBOL)) {
    return -1;
  }
  int i=0;
  while (prim_names[i]) {
    if (strcmp(intern_name(symbol_value(sym)), prim_names[i]) == 0) {
      return i;
    }
    i++;
  }
  return -1;
}

bool is_prim(obj_t *sym)
{
  return (prim_num(sym) != -1);
}

bool is_atom(obj_t *e)
{
  return ((e == NULL) ||
          (obj_is_type(e, OBJ_TYPE_NUM)) ||
          (obj_is_type(e, OBJ_TYPE_STRING)) ||
          (obj_is_type(e, OBJ_TYPE_CHAR)) );
}

bool is_sym(obj_t *e, char *s)
{
  if (obj_is_type(e, OBJ_TYPE_SYMBOL)) {
    return symbol_value(e) == intern(s);
  }
  return false;
}

void comp_pos(comp_t *comp, obj_t *expr)
{
  obj_t *posmap = NULL;
  obj_t *posref = NULL;
  GC_ENTER3(posmap, posref, expr);
  uint32_t pos = comp->cg.code_len;
  if (comp->latest_def && 
      comp->latest_fnid &&
      obj_is_type(expr, OBJ_TYPE_LIST)) {
    dict_get(comp->state->debugmap, NUM_NEW(comp->latest_fnid), &posmap);
    posref = list_new(2);
    list_set_val(posref, 0, NUM_NEW(pos));
    list_set_val(posref, 1, expr);
    posmap = list_cons(posmap, posref);
    dict_set(&comp->state->debugmap, NUM_NEW(comp->latest_fnid), posmap);
  }
  GC_EXIT0();
}

void comp_seq(comp_t *comp, obj_t *seq, int start, env_t *env, bool tail)
{
  obj_t *e = NULL;
  GC_ENTER2(seq, e);
  size_t n = list_size(seq);
  for (int i=start; i<n; i++) {
    e = list_val(seq, i);
    bool last = (i == (n-1));
    comp_expr(comp, e, env, last ? tail : false);
    if (!last) {
      cg_inst0(&comp->cg, VM_OP_SPCL, VM_SPCL_POP);
    }
  }
  GC_EXIT0();
}

void comp_lit(comp_t *comp, obj_t *lit)
{ 
  if (lit == NULL) {
    cg_byte(&comp->cg, VM_LIT_NONE);
  } else if (obj_is_type(lit, OBJ_TYPE_STRING)) {
    uint32_t sz = string_size(lit);
    cg_byte(&comp->cg, VM_LIT_STRING);
    cg_byte(&comp->cg, sz);
    for (int i=0; i<sz; i++) {
      cg_byte(&comp->cg, string_char(lit, i));
    }
  } else if (obj_is_type(lit, OBJ_TYPE_CHAR)) {
    cg_byte(&comp->cg, VM_LIT_CHAR);
    cg_byte(&comp->cg, char_value(lit));
  } else if (obj_is_type(lit, OBJ_TYPE_SYMBOL)) {
    uint32_t sym = symbol_value(lit);
    assert(sym < 65535);
    cg_byte(&comp->cg, VM_LIT_SYMBOL);
    cg_byte(&comp->cg, sym >> 8);
    cg_byte(&comp->cg, sym & 0xff);
  } else if (obj_is_type(lit, OBJ_TYPE_NUM)) {
    if ((num_value(lit) <= 32767) && (num_value(lit) >= -32768)) {
      int16_t val = num_value(lit);
      cg_byte(&comp->cg, VM_LIT_INT16);
      cg_bytes(&comp->cg, (uint8_t*)&val, 2);
    } else if ((num_value(lit) <= 2147483647) && (num_value(lit) >= -2147483648)) {
      int32_t val = num_value(lit);
      cg_byte(&comp->cg, VM_LIT_INT32);
      cg_bytes(&comp->cg, (uint8_t*)&val, 4);
    } else {
      FATAL("integer literal too large");
    }
  } else if (obj_is_type(lit, OBJ_TYPE_FFI)) {
    uint32_t ptr = (uint32_t)ffi_value(lit);
    cg_byte(&comp->cg, VM_LIT_FFI);
    cg_bytes(&comp->cg, (uint8_t*)&ptr, 4);
  } else if (obj_is_type(lit, OBJ_TYPE_LIST)) {
    uint32_t sz = list_size(lit);
    cg_byte(&comp->cg, VM_LIT_LIST);
    cg_byte(&comp->cg, sz);
    for (int i=0; i<sz; i++) {
      comp_lit(comp, list_val(lit, i));
    }
  } else {
    FATAL("Bad literal");
  }
}

void comp_lits(comp_t *comp, obj_t *lits)
{
  if (lits) {
    int p = cg_instj(&comp->cg, VM_OP_JMP, 0);
    cg_inst0(&comp->cg, VM_OP_SPCL, VM_SPCL_LITS);
    for (int i=0; i<lits->size; i+=2) {
      if (lits->data[i]) {
        obj_t *lit = (obj_t*)lits->data[i];
        obj_t *uses = (obj_t*)lits->data[i+1];
        for (int j=0; j<list_size(uses); j++) {
         cg_patch(&comp->cg, NUM_VALUE(list_val(uses, j)));
        }
        comp_lit(comp, lit);
      }
    }
    cg_patch(&comp->cg, p);
  }
}

void comp_lookup(comp_t *comp, obj_t *expr, env_t *env)
{
  int d, n;
  if (env_get(env, expr, &d, &n)) {
    if (d == 0) {
      cg_insts(&comp->cg, VM_OP_AGET0, n);
    } else {
      cg_instp(&comp->cg, VM_OP_AGET, d, n);
    }
  } else if (is_prim(expr)) {
    // primitives aren't first class
    common_error(ERROR_COMP_PRIM_REF);
  } else {
    cg_insts(&comp->cg, VM_OP_GET, symbol_value(expr));
  }
}

void comp_lambda(comp_t *comp, obj_t *expr, env_t *env)
{
  obj_t *args = NULL;
  obj_t *olits = NULL;
  obj_t *fnmap = NULL;
  GC_ENTER5(expr, args, olits, comp->lits, fnmap);
  args = list_val(expr, 1);
  int argcount = 0;
  bool vararg = false;
  env_t *e = env_extend(env, args, &argcount, &vararg);
  vm_op_t inst = vararg ? VM_OP_FNV : VM_OP_FN;
  int p = cg_instj(&comp->cg, inst, argcount);
  comp->latest_fnid = ++comp->state->func_count;
  fnmap = list_cons(NULL, comp->latest_def);
  dict_set(&comp->state->debugmap, NUM_NEW(comp->latest_fnid), fnmap);
  cg_funcid(&comp->cg, comp->latest_fnid);
  olits = comp->lits;
  comp->lits = NULL;
  comp_seq(comp, expr, 2, e, true);
  env_free(e);
  cg_inst0(&comp->cg, VM_OP_SPCL, VM_SPCL_RET);
  comp_lits(comp, comp->lits);
  comp->lits = olits;
  cg_patch(&comp->cg, p);
  GC_EXIT0();
}

void comp_apply(comp_t *comp, obj_t *expr, env_t *env, bool tail)
{
  obj_t *fn = NULL;
  obj_t *expanded = NULL;
  GC_ENTER3(expr, fn, expanded);
  fn = list_val(expr, 0);
  if (is_macro(comp->macros, fn)) {
    expanded = macro_expand(comp->macros, expr);
    comp_expr(comp, expanded, env, tail);
  } else {
    size_t num_args = list_size(expr)-1;
    for (int i=num_args; i>0; i--) {
      comp_expr(comp, list_val(expr, i), env, false);
    }
    if (is_prim(fn)) {
      cg_instp(&comp->cg, VM_OP_PRIM, num_args, prim_num(fn));
    } else {
      comp_expr(comp, fn, env, false);
      cg_insts(&comp->cg, tail ? VM_OP_TCALL : VM_OP_CALL, num_args);
    }
  }
  GC_EXIT0();
}

void comp_builtin(comp_t *comp, obj_t *expr, env_t *env, bool tail)
{
  obj_t *fn = NULL;
  obj_t *name = NULL;
  obj_t *v = NULL;
  GC_ENTER4(expr, fn, name, v);

  fn = list_val(expr, 0);
  if (is_sym(fn, "def")) {
    name = list_val(expr, 1);
    comp->latest_def = name;
    comp_expr(comp, list_val(expr, 2), env, tail);
    comp->latest_def = NULL;
    cg_insts(&comp->cg, VM_OP_SET, symbol_value(name));
    comp->isdef = true;
    name = NULL;
  } else if (is_sym(fn, "defmacro")) {
    name = list_val(expr, 1);
    assert(obj_is_type(name, OBJ_TYPE_SYMBOL));
    comp->latest_def = name;
    comp_expr(comp, list_val(expr, 2), env, tail);
    comp->latest_def = NULL;
    cg_insts(&comp->cg, VM_OP_SET, symbol_value(name));
    set_macro(comp->macros, name);
    name = NULL;
  } else if (is_sym(fn, "set!")) {
    name = list_val(expr, 1);
    comp->latest_def = name;
    comp_expr(comp, list_val(expr, 2), env, tail);
    comp->latest_def = NULL;
    int d, n;
    if (env_get(env, name, &d, &n)) {
      cg_instp(&comp->cg, VM_OP_ASET, d, n);
    } else {
      cg_insts(&comp->cg, VM_OP_SET, symbol_value(name));
    }
    name = NULL;
  } else if (is_sym(fn, "lambda")) {
    comp_lambda(comp, expr, env);
  } else if (is_sym(fn, "quote")) {
    if (is_atom(list_val(expr, 1))) {
      comp_expr(comp, list_val(expr, 1), env, tail);
    } else {
      comp->lits = lit_add_use(comp->lits, list_val(expr, 1), comp->cg.code_len);
      cg_instj(&comp->cg, VM_OP_LIT, 0);
    }
  } else if (is_sym(fn, "if")) {
    comp_expr(comp, list_val(expr, 1), env, false);
    int p0 = cg_instj(&comp->cg, VM_OP_JZ, 0);
    comp_expr(comp, list_val(expr, 2), env, tail);
    int p1 = cg_instj(&comp->cg, VM_OP_JMP, 0);
    cg_patch(&comp->cg, p0);
    if (list_size(expr) == 4) {
      comp_expr(comp, list_val(expr, 3), env, tail);
    } else {
      cg_inst0(&comp->cg, VM_OP_SPCL, VM_SPCL_NIL);
    }
    cg_patch(&comp->cg, p1);
  } else if (is_sym(fn, "begin")) {
    comp_seq(comp, expr, 1, env, tail);
  } else if (is_sym(fn, "macroexpand")) {
    // FIXME: macroexpand should take a quoted form
    v = macro_expand(comp->macros, list_val(expr, 1));
    comp->lits = lit_add_use(comp->lits, v, comp->cg.code_len);
    cg_instj(&comp->cg, VM_OP_LIT, 0);
  } else {
    comp_apply(comp, expr, env, tail);
  }
  GC_EXIT0();
}

void comp_expr(comp_t *comp, obj_t *expr, env_t *env, bool tail)
{
  GC_ENTER1(expr);
  if (expr == NULL) {
    cg_inst0(&comp->cg, VM_OP_SPCL, VM_SPCL_NIL);
  } else if (obj_is_type(expr, OBJ_TYPE_SYMBOL)) {
    comp_lookup(comp, expr, env);
  } else if (obj_is_type(expr, OBJ_TYPE_NUM)) {
    int32_t val = NUM_VALUE(expr);
    if ((val >= -7) && (val <= 255)) {
      cg_insti(&comp->cg, VM_OP_IMM, val);
    } else {
      comp->lits = lit_add_use(comp->lits, expr, comp->cg.code_len);
      cg_instj(&comp->cg, VM_OP_LIT, 0);
    }
  } else if (obj_is_type(expr, OBJ_TYPE_STRING) ||
             obj_is_type(expr, OBJ_TYPE_FFI)) {
    comp->lits = lit_add_use(comp->lits, expr, comp->cg.code_len);
    cg_instj(&comp->cg, VM_OP_LIT, 0);
  } else if (obj_is_type(expr, OBJ_TYPE_LIST)) {
    comp_builtin(comp, expr, env, tail);
  } else {
    FATAL("unhandled comp");
  }
  comp_pos(comp, expr);
  GC_EXIT0();
}

obj_t *comp_compile(comp_state_t *state, obj_t *expr, void *macros, bool *isdef)
{
  comp_t c;
  memset(&c, 0, sizeof(c));
  c.state = state;

  GC_ENTER2(expr, c.lits);
  c.macros = macros;
  cg_new(&c.cg);
  env_t *e = env_new();
  comp_expr(&c, expr, e, true);
  env_free(e);
  comp_lits(&c, c.lits);
  *isdef = c.isdef;
  expr = bytearray_new(c.cg.code_len, c.cg.code);
  GC_EXIT(expr);
}
