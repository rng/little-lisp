#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "common.h"
#include "lex.h"

void lex_init(lex_t *lex, char *s)
{
  lex->pos = 0;
  lex->line = 1;
  lex->linestart = s;
  lex->buf = s;
  lex->cur = s;
  lex->strbuf = (char*)malloc(strlen(s));
}

static void lex_sym(lex_t *lex)
{
  lex->tok.t = TOKEN_SYM;
  lex->tok.s = lex->strbuf;
}

static void lex_str(lex_t *lex)
{
  lex->tok.t = TOKEN_STR;
  lex->tok.s = lex->strbuf;
}

static void lex_int(lex_t *lex)
{
  lex->tok.t = TOKEN_INT;
  lex->tok.i = atoi(lex->strbuf);
}

token_t *lex_next(lex_t *lex)
{
  char *s = lex->cur;
  // eat whitespace
  while (*s && strchr("\n\t\r; ", *s)) {
    if (*s == ';') {
      while (*s && *s != '\n') {
        s++;
      }
    } else if (*s == '\n') {
      lex->line++;
      lex->linestart = s;
      s++;
    } else {
      s++;
    }
  }
  // get token
  if (*s) {
    char c = *s;
    if (strchr("[]{}()'`&,:", c)) {
      s++;
      if ((c == ',') && (*s == '@')) {
        s++;
        strcpy(lex->strbuf, ",@");
        lex_sym(lex);
      } else {
        lex->strbuf[0] = c;
        lex->strbuf[1] = 0;
        lex_sym(lex);
      }
    } else if (c =='"') {
      s++;
      char *r = lex->strbuf;
      while (*s && *s != '"') {
        if (*s == '\\') {
          s++;
          switch (*s++) {
            case 'n': *r++ = '\n'; break;
            case 'r': *r++ = '\r'; break;
            case 't': *r++ = '\t'; break;
            case '\\': *r++ = '\\'; break;
            default: common_error1(ERROR_LEX_BAD_ESCAPE, CHAR_NEW(*s)); break;
          }
        } else {
          *r++ = *s++;
        }
      }
      *r++ = 0;
      s++;
      lex_str(lex);
    } else if (((c >= '0') && (c <= '9')) || (c == '-')) {
      char *r = lex->strbuf;
      if (c == '-') {
        *r++ = *s++;
      }
      if ((*s >= '0') && (*s <= '9')) {
        while ((*s >= '0') && (*s <= '9')) {
          *r++ = *s++;
        }
        *r++ = 0;
        lex_int(lex);
      } else {
        *r++ = 0;
        lex_sym(lex);
      }
    } else if (strchr("_><=+/*~%?|!",c) || isalpha(c)) {
      char *r = lex->strbuf;
      while (*s && (strchr("_><=+-/*~%?|!",*s) || isalnum(*s))) {
        *r++ = *s++;
      }
      *r++ = 0;
      lex_sym(lex);
    } else {
      common_error1(ERROR_LEX_BAD_CHAR, CHAR_NEW(c));
    }
  } else {
    lex->tok.t = TOKEN_EOF;
  }
  lex->cur = s;
  return &lex->tok;
}
