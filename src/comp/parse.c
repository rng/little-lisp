#include "parse.h"
#include "lex.h"
#include "obj.h"
#include "intern.h"
#include "common.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

static obj_t *parse_expr(lex_t *lex);

static bool is_sym(token_t *t, char *sym)
{
  return ((t->t == TOKEN_SYM) && (strcmp(t->s, sym) == 0));
}

static token_t *parse_peek(lex_t *lex)
{
  return &lex->tok;
}

static void parse_accept(lex_t *lex, char *sym)
{
  if (!is_sym(parse_peek(lex), sym)) {
    common_error(ERROR_PARSE_UNEXPECTED);
  }
  lex_next(lex);
}

static obj_t *mk_apply1(char *func, obj_t *expr)
{
  obj_t *r = NULL;
  GC_ENTER2(expr, r);
  r = list_new(2);
  list_set_val(r, 0, symbol_new(intern(func)));
  list_set_val(r, 1, expr);
  GC_EXIT(r);
}

static obj_t *parse_atom(lex_t *lex)
{
  token_t *tok = parse_peek(lex);
  if (is_sym(tok, "nil")) {
    lex_next(lex);
    return NULL;
  } else if (is_sym(tok, "true")) {
    lex_next(lex);
    return NUM_NEW(1);
  } else if (is_sym(tok, "false")) {
    lex_next(lex);
    return NUM_NEW(0);
  } else if (tok->t == TOKEN_INT) {
    int32_t v = tok->i;
    lex_next(lex);
    return NUM_NEW(v);
  } else if (tok->t == TOKEN_STR) {
    obj_t *r = string_new_c(tok->s);
    lex_next(lex);
    return r;
  } else {
    obj_t *r = symbol_new(intern(tok->s));
    lex_next(lex);
    return r;
  }
}

static obj_t *parse_list(lex_t *lex)
{
  obj_t *r = NULL; 
  GC_ENTER1(r);
  parse_accept(lex, "(");
  token_t *tok = parse_peek(lex);
  while (!is_sym(tok, ")")) {
    if ((!tok) || (tok->t == TOKEN_EOF)) {
      common_error(ERROR_PARSE_UNEXPECTED_LIST);
    }
    obj_t *v = parse_expr(lex);
    if (r == NULL) r = list_new(0);
    r = list_cons(r, v);
    tok = parse_peek(lex);
  }
  lex_next(lex);
  GC_EXIT(r);
}

static obj_t *parse_expr(lex_t *lex)
{
  token_t *tok = parse_peek(lex);
  if ((!tok)) {
    common_error(ERROR_PARSE_BAD_TOKEN);
    return NULL;
  } else if (is_sym(tok, "nil")) {
    lex_next(lex);
    return NULL;
  } else if (is_sym(tok, "'")) {
    lex_next(lex);
    return mk_apply1("quote", parse_expr(lex));
  } else if (is_sym(tok, "`")) {
    lex_next(lex);
    return mk_apply1("quasiquote", parse_expr(lex));
  } else if (is_sym(tok, ",")) {
    lex_next(lex);
    return mk_apply1("unquote", parse_expr(lex));
  } else if (is_sym(tok, ",@")) {
    lex_next(lex);
    return mk_apply1("splice-unquote", parse_expr(lex));
  } else if (is_sym(tok, "(")) {
    return parse_list(lex);
  } else {
    return parse_atom(lex);
  }
}

obj_t *parse_parse(char *input)
{
  obj_t *r = NULL;
  lex_t lex;
  lex_init(&lex, input);
  lex_next(&lex);
  r = parse_expr(&lex);
  if (parse_peek(&lex)->t != TOKEN_EOF) {
    common_error(ERROR_PARSE_UNEXPECTED);
  }
  return r;
}
