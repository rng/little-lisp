#include "macro.h"
#include "comp.h"
#include "intern.h"
#include <assert.h>
#include <string.h>

obj_t *macro_eval(macros_t *m, obj_t *code)
{
  GC_ENTER1(code);
  GC_EXIT(vm_run(&m->vm, (bytearray_t*)code));
}

obj_t *macro_expand(macros_t *m, obj_t *expr) {
  obj_t *macrocall = NULL;
  obj_t *q = NULL;
  obj_t *code = NULL;
  GC_ENTER4(expr, macrocall, q, code);

  int argcount = list_size(expr) - 1;
  macrocall = list_new(argcount+1);
  list_set_val(macrocall, 0, list_val(expr, 0));
  for (int i=0; i<argcount; i++) {
    q = list_new(2);
    list_set_val(q, 0, symbol_new(intern("quote")));
    list_set_val(q, 1, list_val(expr, i+1));
    list_set_val(macrocall, i+1, q);
  }
  bool isdef;
  code = comp_compile(m->compstate, macrocall, NULL, &isdef);
  q = macro_eval(m, code);
  GC_EXIT(q);
}

void macro_init(macros_t *m, comp_state_t *compstate)
{
  memset(m, 0, sizeof(macros_t));
  m->compstate = compstate;
  vm_init(&m->vm);
}

bool is_macro(macros_t *m, obj_t *sym)
{
  if (m) {
    for (int i=0; i<m->num_macros; i++) {
      if (m->macros[i] == sym) {
        return true;
      }
    }
  }
  return false;
}

void set_macro(macros_t *m, obj_t *expr)
{
  assert(m);
  assert(obj_is_type(expr, OBJ_TYPE_SYMBOL));
  m->macros[m->num_macros++] = expr;
  assert(m->num_macros < MAX_MACROS);
}
