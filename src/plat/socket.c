#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>

#include "socket.h"
#include "common.h"

#define MAXMSG (512)
#define PORT (50000)

int socket_create()
{
  int sock;

  sock = socket(PF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    FATAL("socket open failed");
  }

  int yes = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
    FATAL("socket reuse failed");
  }

  return sock;
}

void socket_close(int socket)
{
  close(socket);
}

void socket_send(int sock, uint8_t *buf, size_t buf_len)
{
  int bytes_written = write(sock, buf, buf_len);
  if (bytes_written != buf_len) {
    FATAL("%d bytes written (of %zd)\n", bytes_written, buf_len);
  }
}

void server_init(server_t *srv, void *context, server_process_cb_t callback)
{
  struct sockaddr_in name;
  srv->context = context;
  srv->sock = socket_create();
  srv->max_sock = srv->sock;
  srv->callback = callback;

  name.sin_family = AF_INET;
  name.sin_port = htons(PORT);
  name.sin_addr.s_addr = htonl(INADDR_ANY);
  if (bind(srv->sock, (struct sockaddr *) &name, sizeof(name)) < 0) {
    FATAL("bind failed");
  }

  if (listen(srv->sock, 1) < 0) {
    FATAL("listen failed");
  }
  FD_ZERO(&srv->active_fd_set);
  FD_SET(srv->sock, &srv->active_fd_set);

}

void server_send(server_t *srv, uint8_t *buf, size_t buf_len)
{
  socket_send(srv->client_sock, buf, buf_len);
}

static bool server_read(server_t *srv, int sock)
{
  uint8_t buffer[MAXMSG];
  int nbytes;

  srv->client_sock = sock;
  nbytes = read(sock, buffer, MAXMSG);
  if (nbytes > 0) {
    size_t resp_len = 0;
    srv->callback(srv->context, buffer, nbytes, &resp_len);
    socket_send(sock, buffer, resp_len);
    return true;
  }
  return false;
}

void server_tick(server_t *srv, bool blocking)
{
  struct timeval timeout = {0, 0};
  struct timeval *p_timeout = blocking ? NULL : &timeout;

  fd_set read_fd_set = srv->active_fd_set;
  if (select(srv->max_sock+1, &read_fd_set, NULL, NULL, p_timeout) < 0) {
    FATAL("select failed");
  }

  for (int i = 0; i <= srv->max_sock; i++) {
    if (FD_ISSET(i, &read_fd_set)) {
      if (i == srv->sock) {
        struct sockaddr_in clientname;
        socklen_t size = sizeof(clientname);
        int new_sock = accept(srv->sock,
            (struct sockaddr*)&clientname,
            &size);
        if (new_sock > srv->max_sock) {
          srv->max_sock = new_sock;
        }
        if (new_sock < 0) {
          FATAL("accept failed");
        }
        FD_SET(new_sock, &srv->active_fd_set);
      } else {
        if (!server_read(srv, i)) {
          close(i);
          FD_CLR(i, &srv->active_fd_set);
        }
      }
    }
}
}

int client_init(void)
{
  struct sockaddr_in serv_addr;

  memset((char*)&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  int sock = socket_create();
  if (connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
    FATAL("connect failed");
  }
  return sock;
}

void client_shutdown(int client)
{
  socket_close(client);
}

void client_send(int client, uint8_t *buf, size_t buf_len)
{
  socket_send(client, buf, buf_len);
}

int client_recv(int client, uint8_t *buf, size_t buf_len)
{
  return read(client, buf, buf_len);
}
