#include "platform.h"
#include "common.h"
#include "ffi.h"
#include <dlfcn.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>

obj_t *platform_ffi_sym_lookup(obj_t *name)
{
  void *dl = dlopen(NULL, RTLD_LAZY);
  if (!dl) {
    FATAL("can't dlopen ourselves");
  }
  void* func = dlsym(dl, string_value(name));
  dlclose(dl);
  return func ? ffi_new(func): NULL;
}

void platform_fatal(char *msg, ...)
{
  char buf[512];
  va_list(args);
  va_start(args, msg);
  vsprintf(buf, msg, args);
  printf("%s\n", buf);
  exit(-1);
}
