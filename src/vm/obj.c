#include "obj.h"
#include "common.h"
#include "intern.h"
#include "ffi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OBJ_PRINT_MAX_RECURSE (8)

static void obj_check_type(obj_t *o, obj_type_t typ)
{
  if (!obj_is_type(o, typ)) {
    common_error(ERROR_OBJ_INVALID_TYPE);
  }
}

obj_t *obj_new(obj_type_t typ, uint16_t word_size)
{
  obj_t *o = (obj_t*)mem_new(word_size + 1);
  o->typ = typ;
  o->size = word_size;
  return o;
}

obj_type_t obj_type(obj_t *o)
{
  mem_check_obj(o);
  uint32_t u = (uint32_t)o;
  if ((u&3) == OBJ_TYPE_NUM) {
    return OBJ_TYPE_NUM;
  } else if ((u&3) == OBJ_TYPE_SYMBOL) {
    return OBJ_TYPE_SYMBOL;
  } else if ((u&3) == OBJ_TYPE_CHAR) {
    return OBJ_TYPE_CHAR;
  } else if (o == NULL) {
    return OBJ_TYPE_NIL;
  } else if (o->typ == OBJ_TYPE_FORWARD) {
    FATAL("Invalid obj type");
  } else {
    return o->typ;
  }
}

bool obj_is_type(obj_t *o, obj_type_t typ)
{
  return obj_type(o) == typ;
}

uint32_t obj_hash(obj_t *o) {
  uint32_t h = 0;
  switch (obj_type(o)) {
    case OBJ_TYPE_NUM:
    case OBJ_TYPE_NIL:
    case OBJ_TYPE_SYMBOL:
      return (uint32_t)o;
      break;

    case OBJ_TYPE_STRING:
      for (int i=0; i<string_size(o); i++) {
        h = (h<<7)^string_char(o, i);
      }
      return h;
      break;

    case OBJ_TYPE_BYTEARRAY:
      for (int i=0; i<bytearray_size(o); i++) {
        h = (h<<7)^bytearray_val(o, i);
      }
      return h;
      break;

    case OBJ_TYPE_LIST:
      for (int i=0; i<list_size(o); i++) {
        h += obj_hash(list_val(o, i));
      }
      return h;
      break;

    case OBJ_TYPE_FFI:
      return o->data[0];
      break;

    default:
      FATAL("Unhandled hash %d", obj_type(o));
  }
}

bool obj_cmp(obj_t *a, obj_t *b)
{
  if (obj_type(a) == obj_type(b)) {
    switch (obj_type(a)) {
      case OBJ_TYPE_NIL:
      case OBJ_TYPE_NUM:
      case OBJ_TYPE_CHAR:
      case OBJ_TYPE_SYMBOL:
        return a == b;

      case OBJ_TYPE_STRING:
        if (a->size == b->size) {
          return (memcmp(a->data, b->data, a->size*4) == 0);
        }
        return false;

      case OBJ_TYPE_LIST:
        if (a->size == b->size) {
          for (int i=0; i<a->size; i++) {
            if (!obj_cmp(list_val(a, i), list_val(b, i))) {
              return false;
            }
          }
          return true;
        }
        return false;

      default:
        FATAL("Unhandled cmp %d", obj_type(a));
    }
  }
  return false;
}

obj_t *obj_to_string_rec(obj_t *o, uint32_t visited[], int visit_count, bool repr)
{
  char tmp[32];
  uint32_t s;
  obj_t *r = NULL;
  GC_ENTER2(r, o);

  // check for recursive objects
  uint32_t hash = obj_hash(o);
  if (visit_count == OBJ_PRINT_MAX_RECURSE) {
    r = string_new_c("[Too many levels]");
    goto done;
  } else if (o != NULL) {
    for (int i=0; i<visit_count; i++) {
      if (hash == visited[i]) {
        r = string_new_c("[Recursive]");
        goto done;
      }
    }
  }
  visited[visit_count++] = hash;

  switch(obj_type(o)) {
    case OBJ_TYPE_NIL:
      r = string_new_c("nil");
      break;

    case OBJ_TYPE_NUM:
      s = snprintf(tmp, sizeof(tmp), "%d", NUM_VALUE(o));
      r = string_new(s, tmp);
      break;

    case OBJ_TYPE_CHAR:
      r = string_new(3, NULL);
      ((bytearray_t*)r)->data[0] = '#';
      ((bytearray_t*)r)->data[1] = '\\';
      ((bytearray_t*)r)->data[2] = char_value(o);
      break;

    case OBJ_TYPE_STRING:
      s = string_size(o);
      if (repr) {
        r = string_new(s+2, NULL);
        ((bytearray_t*)r)->data[0] = '\'';
        ((bytearray_t*)r)->data[s+1] = '\'';
        memcpy(((bytearray_t*)r)->data+1, ((bytearray_t*)o)->data, s);
      } else {
        r = string_new(s, NULL);
        memcpy(((bytearray_t*)r)->data, ((bytearray_t*)o)->data, s);
      }
      break;

    case OBJ_TYPE_SYMBOL:
      r = string_new_c(intern_name(symbol_value(o)));
      break;

    case OBJ_TYPE_LIST:
      r = string_new_c("(");
      s = list_size(o);
      for (int i=0; i<s; i++) {
        r = string_append(r, obj_to_string_rec(list_val(o, i), visited, visit_count, repr));
        if (i != (s-1)) {
          r = string_append(r, string_new_c(" "));
        }
      }
      r = string_append(r, string_new_c(")"));
      break;

    case OBJ_TYPE_DICT:
      r = string_new_c("{");
      for (int i=0; i<o->size; i+=2) {
        if (o->data[i]) {
          r = string_append(r, obj_to_string_rec((obj_t*)o->data[i], visited, visit_count,repr));
          r = string_append(r, string_new_c(":"));
          r = string_append(r, obj_to_string_rec((obj_t*)o->data[i+1], visited, visit_count, repr));
          r = string_append(r, string_new_c(", "));
        }
      }
      r = string_append(r, string_new_c("}"));
      break;

    case OBJ_TYPE_FUNC:
      s = snprintf(tmp, sizeof(tmp), "<FN %08x>", (uint32_t)o);
      r = string_new(s, tmp);
      break;

    case OBJ_TYPE_BYTEARRAY:
      s = bytearray_size(o);
      r = string_new(s*3 + 3, NULL);
      ((bytearray_t*)r)->data[0] = '[';
      ((bytearray_t*)r)->data[1] = ' ';
      for (int i=0; i<s; i++) {
        int ofs = (s*3) + 2;
        snprintf((char*)((bytearray_t*)r)->data+ofs, 3, "%02x ", ((bytearray_t*)o)->data[i]);
      }
      ((bytearray_t*)r)->data[s*3+2] = ']';
      break;

    case OBJ_TYPE_FFI:
      s = snprintf(tmp, sizeof(tmp), "<FFI 0x%08x>", (uint32_t)ffi_value(o));
      r = string_new(s, tmp);
      break;

    default:
      FATAL("Unhandled print %d", obj_type(o));
  }

done:
  GC_EXIT(r);
}

obj_t *obj_to_string(obj_t *o)
{
  obj_t *r = NULL;
  GC_ENTER2(o, r);
  uint32_t visited[OBJ_PRINT_MAX_RECURSE];
  memset(visited, 0, sizeof(visited));
  r = obj_to_string_rec(o, visited, 0, true);
  GC_EXIT(r);
}

obj_t *obj_to_string_raw(obj_t *o)
{
  obj_t *r = NULL;
  GC_ENTER2(o, r);
  uint32_t visited[OBJ_PRINT_MAX_RECURSE];
  memset(visited, 0, sizeof(visited));
  r = obj_to_string_rec(o, visited, 0, false);
  GC_EXIT(r);
}

obj_t *func_new(uint16_t argcount,
                bool varargs,
                uint16_t fnid,
                obj_t *frame,
                obj_t *code)
{
  GC_ENTER2(frame, code);
  func_t *f = (func_t*)obj_new(OBJ_TYPE_FUNC, 3);
  f->argcount = argcount;
  f->varargs = varargs;
  f->fnid = fnid;
  f->frame = frame;
  f->code = code;
  GC_EXIT((obj_t*)f);
}

uint16_t func_argcount(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_FUNC);
  func_t *f = (func_t*)o;
  return f->argcount;
}

uint16_t func_fnid(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_FUNC);
  func_t *f = (func_t*)o;
  return f->fnid;
}

obj_t *func_frame(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_FUNC);
  func_t *f = (func_t*)o;
  return f->frame;
}

bool func_varargs(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_FUNC);
  func_t *f = (func_t*)o;
  return f->varargs;
}

obj_t *func_code(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_FUNC);
  func_t *f = (func_t*)o;
  return f->code;
}

obj_t *frame_new(obj_t *parent, uint32_t sz)
{
  GC_ENTER1(parent);
  frame_t *f = (frame_t*)obj_new(OBJ_TYPE_FRAME, sz+1);
  f->parent = parent;
  GC_EXIT((obj_t*)f);
}

obj_t *frame_val(obj_t *o, uint32_t idx)
{
  frame_t *f = (frame_t*)o;
  obj_check_type(o, OBJ_TYPE_FRAME);
  OBJ_CHECK_BOUNDS(idx, f->size-1);
  return f->vals[idx];
}

void frame_set_val(obj_t *o, uint32_t idx, obj_t *v)
{
  frame_t *f = (frame_t*)o;
  obj_check_type(o, OBJ_TYPE_FRAME);
  OBJ_CHECK_BOUNDS(idx, f->size-1);
  f->vals[idx] = v;
}

obj_t *frame_deep_val(obj_t *o, uint32_t depth, uint32_t idx)
{
  frame_t *f = (frame_t*)o;
  for (int i=0;i<depth;i++) {
    obj_check_type((obj_t*)f, OBJ_TYPE_FRAME);
    f = (frame_t*)f->parent;
  }
  obj_check_type((obj_t*)f, OBJ_TYPE_FRAME);
  OBJ_CHECK_BOUNDS(idx, f->size-1);
  return f->vals[idx];
}

void frame_deep_set_val(obj_t *o, uint32_t depth, uint32_t idx, obj_t *v)
{
  frame_t *f = (frame_t*)o;
  for (int i=0;i<depth;i++) {
    obj_check_type((obj_t*)f, OBJ_TYPE_FRAME);
    f = (frame_t*)f->parent;
  }
  OBJ_CHECK_BOUNDS(idx, f->size-1);
  f->vals[idx] = v;
}

obj_t *list_new(uint16_t size)
{
  if (size == 0) {
    return NULL;
  }
  return obj_new(OBJ_TYPE_LIST, size);
}

uint32_t list_size(obj_t *l)
{
  if (obj_is_type(l, OBJ_TYPE_NIL)) {
    return 0;
  }
  obj_check_type(l, OBJ_TYPE_LIST);
  return l->size;
}

obj_t *list_val(obj_t *l, uint32_t idx)
{
  obj_check_type(l, OBJ_TYPE_LIST);
  OBJ_CHECK_BOUNDS(idx, l->size);
  return (obj_t*)l->data[idx];
}

void list_set_val(obj_t *l, uint32_t idx, obj_t *v)
{
  obj_check_type(l, OBJ_TYPE_LIST);
  OBJ_CHECK_BOUNDS(idx, l->size);
  l->data[idx] = (uint32_t)v;
}

obj_t *list_append(obj_t *l1, obj_t *l2)
{
  GC_ENTER2(l1, l2);
  obj_check_type(l1, OBJ_TYPE_LIST);
  if (obj_is_type(l2, OBJ_TYPE_NIL)) {
    GC_EXIT(l1);
  } else if (obj_is_type(l2, OBJ_TYPE_LIST)) {
    obj_t *r = list_new(l1->size + l2->size);
    size_t l1_size = l1->size*sizeof(uint32_t);
    size_t l2_size = l2->size*sizeof(uint32_t);
    memcpy(r->data, l1->data, l1_size);
    memcpy(r->data+l1->size, l2->data, l2_size);
    GC_EXIT(r);
  }
  common_error(ERROR_OBJ_INVALID_TYPE);
  GC_EXIT(NULL);
}

obj_t *list_cons(obj_t *l1, obj_t *v)
{
  obj_t *r = NULL;
  GC_ENTER2(l1, v);
  if (obj_is_type(l1, OBJ_TYPE_NIL)) {
    r = list_new(1);
    list_set_val(r, 0, v);
  } else {
    obj_check_type(l1, OBJ_TYPE_LIST);
    r = list_new(l1->size + 1);
    size_t l1_size = l1->size*sizeof(uint32_t);
    memcpy(r->data, l1->data, l1_size);
    list_set_val(r, l1->size, v);
  }
  GC_EXIT(r);
}

obj_t *bytearray_new(uint16_t size, uint8_t *data)
{
  uint16_t word_size = (((size+3)&~3) >> 2) + 1;
  bytearray_t *b = (bytearray_t*)obj_new(OBJ_TYPE_BYTEARRAY, word_size);
  b->bytesize = size;
  if (data) {
    memcpy(b->data, data, size);
  }
  return (obj_t*)b;
}

uint32_t bytearray_size(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_BYTEARRAY);
  bytearray_t *b = (bytearray_t*)o;
  return b->bytesize;
}

uint32_t bytearray_val(obj_t *o, int idx)
{
  obj_check_type(o, OBJ_TYPE_BYTEARRAY);
  bytearray_t *b = (bytearray_t*)o;
  OBJ_CHECK_BOUNDS(idx, b->bytesize);
  return b->data[idx];
}

obj_t *string_new(uint16_t size, char *data)
{
  uint16_t word_size = (((size+1+3)&~3) >> 2) + 1;
  bytearray_t *b = (bytearray_t*)obj_new(OBJ_TYPE_STRING, word_size);
  b->bytesize = size;
  if (data) {
    memcpy(b->data, data, size);
  }
  return (obj_t*)b;
}

obj_t *string_new_c(char *data)
{
  return string_new(strlen(data), data);
}

char *string_value(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_STRING);
  bytearray_t *b = (bytearray_t*)o;
  return (char*)b->data;
}

uint32_t string_size(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_STRING);
  bytearray_t *b = (bytearray_t*)o;
  return b->bytesize;
}

char string_char(obj_t *o, uint32_t idx)
{
  obj_check_type(o, OBJ_TYPE_STRING);
  bytearray_t *b = (bytearray_t*)o;
  OBJ_CHECK_BOUNDS(idx, b->bytesize);
  return b->data[idx];
}

obj_t *string_append(obj_t *a, obj_t *b)
{
  obj_t *r = NULL;
  GC_ENTER3(r, a, b);
  obj_check_type(a, OBJ_TYPE_STRING);
  obj_check_type(b, OBJ_TYPE_STRING);
  uint32_t sz1 = string_size(a);
  uint32_t sz2 = string_size(b);
  r = string_new(sz1 + sz2, NULL);
  memcpy(((bytearray_t*)r)->data,       ((bytearray_t*)a)->data, sz1);
  memcpy(((bytearray_t*)r)->data + sz1, ((bytearray_t*)b)->data, sz2);
  GC_EXIT(r);
}

obj_t *symbol_new(uint32_t value)
{
  uint32_t u = (value<<2)|OBJ_TYPE_SYMBOL;
  return (obj_t*)u;
}

int32_t num_value(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_NUM);
  return ((int32_t)o)>>2;
}

uint32_t symbol_value(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_SYMBOL);
  return ((uint32_t)o)>>2;
}

char char_value(obj_t *o)
{
  obj_check_type(o, OBJ_TYPE_CHAR);
  return (char)(((uint32_t)o)>>2);
}

