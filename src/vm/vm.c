#include <stdio.h>
#include <assert.h>
#include "vm.h"
#include "ffi.h"
#include "dict.h"
#include "prim.h"
#include "string.h"
#include "intern.h"
#include "literal.h"
#include "ops.h"

const char* opnames[] = {"get","set","aget","aget","aset","imm","lit","jmp",
                         "jz","prim","call","tcal","fn",  "fnv","spcl"};

static void vm_stack_init(vm_stack_t *s, uint32_t size)
{
  s->size = size;
  s->top = 0;
  s->buf = list_new(size);
}

void vm_stack_push(vm_t *vm, vm_stack_t *s, obj_t *v)
{
  if (s->top == s->size) {
    common_error(ERROR_VM_STACK_OVERFLOW);
  }
  s->buf->data[s->top++] = (uint32_t)v;
}

obj_t *vm_stack_pop(vm_t *vm, vm_stack_t *s)
{
  if (s->top == 0) {
    common_error(ERROR_VM_STACK_UNDERFLOW);
  }
  obj_t *r = (obj_t*)s->buf->data[--s->top];
  s->buf->data[s->top] = 0;
  return r;
}

size_t vm_stack_size(vm_stack_t *s)
{
  return s->top;
}

static void vm_build_frame(vm_t *vm, obj_t *frame, obj_t *func, uint8_t argcount)
{
  GC_ENTER2(func, frame);

  if ((!func_varargs(func)) && (func_argcount(func) != argcount)) {
    common_error(ERROR_VM_FUNC_ARG_COUNT);
  }
  // build frame
  if (func_varargs(func)) {
    int fixedarg_count = func_argcount(func) - 1;
    int vararg_count = argcount - fixedarg_count;
    for (int i=0; i<fixedarg_count; i++) {
      frame_set_val(frame, i, vm_stack_pop(vm, &vm->ds));
    }
    obj_t *rest = list_new(vararg_count);
    for (int j=0; j<vararg_count; j++) {
      list_set_val(rest, j, vm_stack_pop(vm, &vm->ds));
    }
    frame_set_val(frame, fixedarg_count, rest);
  } else {
    for (int i=0; i<func_argcount(func); i++) {
      frame_set_val(frame, i, vm_stack_pop(vm, &vm->ds));
    }
  }
  GC_EXIT0();
}

static void vm_call(vm_t *vm, uint8_t argcount, bool tail)
{
  obj_t *f = NULL;
  obj_t *func = vm_stack_pop(vm, &vm->ds);
  GC_ENTER2(func, f);
  // check func
  if (!obj_is_type(func, OBJ_TYPE_FUNC)) {
    common_error(ERROR_VM_BAD_FUNC);
  }
  // build a new frame with calling args
  f = frame_new(func_frame(func), func_argcount(func));
  vm_build_frame(vm, f, func, argcount);
  // push current state if we're doing a non-tail call
  if ((!vm->frame) || (!tail)) {
    vm_stack_push(vm, &vm->cs, func);
    vm_stack_push(vm, &vm->cs, vm->frame);
    vm_stack_push(vm, &vm->cs, NUM_NEW(vm->pc));
    vm_stack_push(vm, &vm->cs, (obj_t*)vm->code);
  }
  // switch to new code/frame
  vm->code = (bytearray_t*)func_code(func);
  vm->codesize = bytearray_size((obj_t*)vm->code);
  vm->frame = f;
  vm->pc = 0;
  GC_EXIT0();
}

void vm_init(vm_t *vm)
{
  memset(vm, 0, sizeof(vm_t));
  vm->env = dict_new(32);
  vm->running = false;
}

void vm_reset(vm_t *vm)
{
  vm->pc = 0;
  vm->codesize = 0;
  vm->code = NULL;
  vm->frame = NULL;
  vm->ds.buf = NULL;
  vm->cs.buf = NULL;
  GC_ENTER2(vm->ds.buf, vm->cs.buf);
  vm_stack_init(&vm->ds, VM_DS_SIZE);
  vm_stack_init(&vm->cs, VM_CS_SIZE);
  GC_EXIT0();
}

obj_t* vm_load_literal(vm_t *vm, uint8_t addr, uint8_t *size)
{
  uint8_t sz=0, count;
  obj_t *ret = NULL;
  obj_t *v = NULL;
  GC_ENTER3(ret, v, vm->env);

  vm_literal_t typ = vm->code->data[addr];
  literal_t *lit = (literal_t*)&vm->code->data[addr+1];
  switch (typ) {
    case VM_LIT_STRING:
      count = vm->code->data[addr+1];
      ret = string_new(count, (char*)&vm->code->data[addr+2]);
      sz = count + 2;
      break;

    case VM_LIT_CHAR:
      ret = CHAR_NEW(vm->code->data[addr+1]);
      sz = 2;
      break;

    case VM_LIT_SYMBOL:
      ret = symbol_new((vm->code->data[addr+1]<<8)|vm->code->data[addr+2]);
      sz = 3;
      break;

    case VM_LIT_INT16:
      ret = NUM_NEW(lit->i16);
      sz = 3;
      break;

    case VM_LIT_INT32:
      ret = NUM_NEW(lit->i32);
      sz = 5;
      break;
    
    case VM_LIT_FFI:
      ret = ffi_new(lit->vp);
      sz = 5;
      break;

    case VM_LIT_LIST:
      count = vm->code->data[addr+1];
      ret = list_new(count);
      sz = 2;
      for (int i=0; i<count; i++) {
        uint8_t elt_size = 0;
        v = vm_load_literal(vm, addr + sz, &elt_size);
        list_set_val(ret, i, v);
        sz += elt_size;
      }
      break;

    case VM_LIT_NONE:
      ret = NULL;
      sz = 1;
      break;

    default:
      FATAL("bad literal");
  }
  if (size) {
    *size = sz;
  }
  GC_EXIT(ret);
}

void vm_load_code(vm_t *vm, bytearray_t *code)
{
  vm->code = code;
  vm->codesize = bytearray_size((obj_t*)vm->code);
}

#define IMM(n) (((n) == 15) ? symbol_new(vm->code->data[vm->pc++]) : symbol_new((n)))
#define SIMM(n) (((n) == 15) ? NUM_NEW(vm->code->data[vm->pc++]) : NUM_NEW((n-7)))

bool vm_exec(vm_t *vm, int max_insts)
{
  obj_t *v = NULL;
  obj_t *func = NULL;
  uint8_t n;
  int insts = 0;

  GC_ENTER5(vm->code, vm->env, vm->frame, v, func);

  while ((vm->running) &&
         (vm->pc < vm->codesize) &&
         ((max_insts == -1) || (insts < max_insts))) {
    uint8_t b = vm->code->data[vm->pc++];
    vm_op_t op = b>>4;
    uint8_t opn = b&0xf;
    LOG_DEBUG("%2d [%2zd]: %02x %4s %2d\n", vm->pc-1, vm_stack_size(&vm->ds), b, opnames[op], opn);

    switch (op) {
      case VM_OP_JMP:
        vm->pc++;
        vm->pc += vm->code->data[vm->pc-1]-1;
        break;

      case VM_OP_JZ:
        vm->pc++;
        if (!NUM_VALUE(vm_stack_pop(vm, &vm->ds))) {
          vm->pc += vm->code->data[vm->pc-1]-1;
        }
        break;

      case VM_OP_FN:
      case VM_OP_FNV: {
        const bool varargs = op == VM_OP_FNV;
        uint8_t fn_len = vm->code->data[vm->pc++];
        uint16_t fn_id = (vm->code->data[vm->pc]<<8)|vm->code->data[vm->pc+1];
        uint8_t *fn_start = vm->code->data + vm->pc + 2;
        v = bytearray_new(fn_len, fn_start);
        func = func_new(opn, varargs, fn_id, vm->frame, v);
        vm_stack_push(vm, &vm->ds, func);
        vm->pc += fn_len-1;
        break;
      }

      case VM_OP_SET:
        v = vm_stack_pop(vm, &vm->ds);
        dict_set(&vm->env, IMM(opn), v);
        vm_stack_push(vm, &vm->ds, v);
        break;

      case VM_OP_GET: {
        bool found = dict_get(vm->env, IMM(opn), &v);
        if (!found) {
          common_error1(ERROR_VM_UNKNOWN_GLOBAL, symbol_new(vm->code->data[vm->pc-1]));
        }
        vm_stack_push(vm, &vm->ds, v);
        break;
      }

      case VM_OP_AGET0:
        vm_stack_push(vm, &vm->ds, frame_val(vm->frame, symbol_value(IMM(opn))));
        break;

      case VM_OP_AGET:
        n = vm->code->data[vm->pc++];
        vm_stack_push(vm, &vm->ds, frame_deep_val(vm->frame, opn, n));
        break;

      case VM_OP_ASET:
        n = vm->code->data[vm->pc++];
        v = vm_stack_pop(vm, &vm->ds);
        frame_deep_set_val(vm->frame, opn, n, v);
        vm_stack_push(vm, &vm->ds, v);
        break;

      case VM_OP_IMM:
        vm_stack_push(vm, &vm->ds, SIMM(opn));
        break;

      case VM_OP_LIT:
        vm->pc++;
        n = vm->pc + vm->code->data[vm->pc-1]-1;
        vm_stack_push(vm, &vm->ds, vm_load_literal(vm, n, NULL));
        break;

      case VM_OP_TCALL:
        vm_call(vm, opn, true);
        break;

      case VM_OP_CALL:
        vm_call(vm, opn, false);
        break;

      case VM_OP_PRIM:
        n = vm->code->data[vm->pc++];
        vm_stack_push(vm, &vm->ds, prim_call(vm, n, opn));
        break;

      case VM_OP_SPCL:
        switch (opn) {
          case VM_SPCL_RET:
            vm->code = (bytearray_t*)vm_stack_pop(vm, &vm->cs);
            vm->codesize = bytearray_size((obj_t*)vm->code);
            vm->pc = NUM_VALUE(vm_stack_pop(vm, &vm->cs));
            vm->frame = vm_stack_pop(vm, &vm->cs);
            vm_stack_pop(vm, &vm->cs);
            break;

          case VM_SPCL_NIL:
            vm_stack_push(vm, &vm->ds, NULL);
            break;

          case VM_SPCL_TRUE:
            vm_stack_push(vm, &vm->ds, NUM_NEW(1));
            break;

          case VM_SPCL_FALSE:
            vm_stack_push(vm, &vm->ds, NUM_NEW(0));
            break;

          case VM_SPCL_POP:
            vm_stack_pop(vm, &vm->ds);
            break;

          default:
            common_error(ERROR_VM_BAD_OP);
        }
        break;

      default:
        common_error(ERROR_VM_BAD_OP);
    }
    insts++;
  }
  bool done = !((vm->pc < vm->codesize) && vm->running);
  GC_EXIT(done);
}

obj_t *vm_get_result(vm_t *vm)
{
  if (vm_stack_size(&vm->ds)) {
    return vm_stack_pop(vm, &vm->ds);
  }
  return NULL;
}

obj_t *vm_backtrace(vm_t *vm, common_error_t err)
{
  bytearray_t *b = NULL;
  GC_ENTER2(b, vm->cs.buf);
  int s = vm_stack_size(&vm->cs) / 4;
  b = (bytearray_t*)bytearray_new((s+1)*3 + 2, NULL);
  b->data[0] = err;
  b->data[1] = 0;
  b->data[2] = 0;
  b->data[3] = 0;
  b->data[4] = vm->pc;
  for (int i=0; i<s; i++) {
    uint16_t fnid = func_fnid((obj_t*)vm->cs.buf->data[i*4 + 0]);
    b->data[5+i*3  ] = fnid>>8;
    b->data[5+i*3+1] = fnid&0xff;
    b->data[5+i*3+2] = NUM_VALUE((obj_t*)vm->cs.buf->data[i*4 + 2]);
  }
  GC_EXIT((obj_t*)b);
}

obj_t *vm_run(vm_t *vm, bytearray_t *code)
{
  obj_t *result = NULL;
  GC_ENTER4(code, result, vm->ds.buf, vm->cs.buf);
  vm_reset(vm);
  vm_load_code(vm, code);
  vm->running = true;
  vm_exec(vm, -1);
  vm->running = false;
  result = vm_get_result(vm);
  GC_EXIT(result);
}

void vm_set_running(vm_t *vm, bool running)
{
  vm->running = running;
}

bool vm_running(vm_t *vm)
{
  return vm->running;
}
