#include "common.h"
#include <stdio.h>
#include <stdlib.h>

jmp_buf _exception_jb;
obj_t *_exception_obj;

const char *common_error_str(common_error_t err)
{
  const char *errs[] = {
    "No error",
    "Unknown global",
    "Unexpected number of arguments to primitive",
    "Invalid argument type for primitive",
    "ERROR_VM_BAD_PRIM",
    "ERROR_VM_BAD_OP",
    "Tried to apply a non-function object",
    "Unexpected number of arguments",
    "Division by zero",
    "Error thrown",
    "Stack overflow",
    "Stack underflow",
    "Cannot reference primtive",
    "Bad parse token",
    "Unexpeted token during parse",
    "Missing matching ')'",
    "Invalid object type",
    "List index out of bounds",
    "Unexpected escape character",
    "Unexpected character",
    "Too many arguments to FFI",
    "Bad FFI signature",
    "Bad type to FFI",
  };
  return errs[err];
}

void common_error(common_error_t err)
{
  _exception_obj = NULL;
  longjmp(_exception_jb, err);
}

void common_error1(common_error_t err, obj_t *obj)
{
  _exception_obj = obj;
  longjmp(_exception_jb, err);
}

