#include "dict.h"
#include "common.h"

obj_t *dict_new(uint32_t size)
{
  return obj_new(OBJ_TYPE_DICT, size*2);
}

obj_t *dict_resize(obj_t *d) {
  GC_ENTER1(d);
  obj_t *new = dict_new(d->size * 2);
  for (int i=0; i<d->size; i+=2) {
    obj_t *k = (obj_t*)d->data[i    ];
    obj_t *v = (obj_t*)d->data[i + 1];
    if (k || v) {
      dict_set(&new, k, v);
    }
  }
  GC_EXIT(new);
}

void dict_set(obj_t **d, obj_t *k, obj_t *v)
{
  obj_t *dict = *d;
  GC_ENTER3(dict, k, v);
  uint32_t idx = obj_hash(k)*2;
  for (int i=0; i<dict->size; i+=2) {
    uint32_t ofs = (idx + i) % dict->size;
    OBJ_CHECK_BOUNDS(ofs+1, dict->size);
    if ((!dict->data[ofs]) || obj_cmp((obj_t*)dict->data[ofs], k)) {
      dict->data[ofs    ] = (uint32_t)k;
      dict->data[ofs + 1] = (uint32_t)v;
      GC_EXIT0();
      return;
    }
  }
  *d = dict_resize(dict);
  dict_set(d, k, v);
  GC_EXIT0();
}

bool dict_get(obj_t *d, obj_t *k, obj_t **v)
{
  uint32_t idx = obj_hash(k)*2;
  for (int i=0; i<d->size; i+=2) {
    uint32_t ofs = (idx + i) % d->size;
    if (obj_cmp((obj_t*)d->data[ofs], k)) {
      *v = (obj_t*)d->data[ofs + 1];
      return true;
    }
  }
  return false;
}

