#include "mem.h"
#include "common.h"
#include <string.h>
#include <assert.h>

gc_stack_t *_gc_roots = NULL;

typedef struct {
  uint32_t *heap;
  uint32_t *heap_to;
  size_t heap_size;
  uint32_t *next_free;
} mem_t;
mem_t mem;

void mem_init(uint32_t *heaps, size_t heap_word_size)
{
  assert((heap_word_size%2) == 0);
  memset(&mem, 0, sizeof(mem));
  mem.heap_size = heap_word_size/2;
  mem.heap = heaps;
  mem.heap_to = &heaps[mem.heap_size];
  memset(mem.heap, 0, sizeof(uint32_t)*mem.heap_size);
  mem.next_free = mem.heap;
  _gc_roots = NULL;
}

void mem_check_obj(obj_t *o)
{
  if (o != NULL) {
    uint32_t *u = (uint32_t*)o;
    if ((((uint32_t)u)&3) == 0) {
      assert(u >= mem.heap);
      assert(u < (mem.heap+mem.heap_size));
    }
  }
}

static bool mem_is_ptr(uint32_t *u)
{
  return (u != NULL) && ((((uint32_t)u)&3) == 0);
}

static bool mem_in_from(uint32_t *p) {
  return ((p >= mem.heap) && (p < (mem.heap+mem.heap_size)));
}

static bool mem_in_to(uint32_t *p) {
  return ((p >= mem.heap_to) && (p < (mem.heap_to+mem.heap_size)));
}

static uint32_t *mem_copy_forward(uint32_t *u)
{
  if (!mem_is_ptr(u)) {
    return u;
  }

  if (mem_in_from(u)) {
    obj_t *o = (obj_t*)u;
    if (o->typ == OBJ_TYPE_FORWARD) {
      // if already forwarded return forward pointer
      assert(mem_in_to((uint32_t*)o->data[0]));
      return (uint32_t*)o->data[0];
    } else {
      if ((o->typ < OBJ_TYPE_FUNC) || (o->typ >= OBJ_TYPE_MAX)) {
        FATAL("bad obj type=%d sz=%d (@%08x)", o->typ, o->size, (uint32_t)u);
      }
      assert(o->size < mem.heap_size/sizeof(uint32_t));
      // copy obj
      obj_t *n = (obj_t*)mem.next_free;
      mem.next_free += (o->size+1);
      assert(mem.next_free < (mem.heap_to+mem.heap_size));
      memcpy(n, o, sizeof(uint32_t)*(o->size+1));
      // forward
      assert(o->size>0);
      o->data[0] = (uint32_t)n;
      o->typ = OBJ_TYPE_FORWARD;
      assert(mem_in_to((uint32_t*)n));
      return (uint32_t*)n;
    }
  }
  assert(mem_in_to(u));
  return u;
}

#ifdef DEBUG
void mem_dump(uint32_t *heap, uint32_t size)
{
  uint32_t *ptr = heap;
  while (ptr < (heap+size)) {
    uint32_t u = (uint32_t)*ptr;
    LOG_DEBUG("%08x: [%08x] ", (uint32_t)ptr, u);
    {
      obj_t *o = (obj_t*)ptr;
      char *names[] = {0,0,0,0,"fwd  ","func ","frame","list ","strng","bytes","dict "};
      if (o->typ == 0) {
        break;
      }
      LOG_DEBUG("obj %-6s %-2d [", names[o->typ], o->size);
      for (int i=0;i<o->size;i++) {
        LOG_DEBUG(" %08x", o->data[i]);
      }
      LOG_DEBUG(" ]");
      ptr += o->size+1;
    }
    LOG_DEBUG("\n");
  }
  LOG_DEBUG("\n");
}
#endif

void mem_gc()
{
  LOG_DEBUG("gc %08x-%08x\n", (uint32_t)mem.heap, (uint32_t)mem.next_free);
  //mem_dump(mem.heap, mem.heap_size);
  // zero new heap
  memset(mem.heap_to, 0, sizeof(uint32_t)*mem.heap_size);

  mem.next_free = mem.heap_to;

  // collect from roots
  gc_stack_t *stk = _gc_roots;
  while (stk) {
    for (int i=0; stk->roots[i]; i++) {
      uint32_t **root = (uint32_t**)stk->roots[i];
      *root = mem_copy_forward(*root);
    }
    stk = stk->parent;
  }

  // scan new heap to process roots
  uint32_t *scan = mem.heap_to;
  while (scan < mem.next_free) {
    obj_t *o = (obj_t*)scan;
    assert ((o->typ >= OBJ_TYPE_FUNC) && (o->typ < OBJ_TYPE_MAX));
    assert (o->size < 1024);

    switch (o->typ) {
      case OBJ_TYPE_BYTEARRAY:
      case OBJ_TYPE_STRING:
      case OBJ_TYPE_FFI:
        break;

      case OBJ_TYPE_FUNC:
        for (int i=1; i<o->size; i++) {
          o->data[i] = (uint32_t)mem_copy_forward((uint32_t*)o->data[i]);
          assert(mem_in_to((uint32_t*)o->data[i]) || !mem_is_ptr((uint32_t*)o->data[i]));
        }
        break;

      default:
        for (int i=0; i<o->size; i++) {
          o->data[i] = (uint32_t)mem_copy_forward((uint32_t*)o->data[i]);
          assert(mem_in_to((uint32_t*)o->data[i]) || !mem_is_ptr((uint32_t*)o->data[i]));
        }
    }
    scan += (o->size+1);
  }

  // swap spaces
  uint32_t *tmp = mem.heap;
  mem.heap = mem.heap_to;
  mem.heap_to = tmp;
  assert(mem.heap != mem.heap_to);

  LOG_DEBUG("GC complete %3d/%d\n", mem.next_free-mem.heap, mem.heap_size);
  LOG_DEBUG("ok %08x-%08x\n", (uint32_t)mem.heap, (uint32_t)mem.next_free);
  //mem_dump(mem.heap, mem.heap_size);
  //LOG_DEBUG("new roots:\n");
  /* stk = _gc_roots; */
  /* j = 0; */
  /* while (stk) { */
  /*   LOG_DEBUG("| %2d %-12s:", j++, stk->name); */
  /*   for (int i=0; stk->roots[i]; i++) { */
  /*     uint32_t **root = (uint32_t**)stk->roots[i]; */
  /*     LOG_DEBUG(" %08x", (uint32_t)*root); */
  /*     if (*root != 0) { */
  /*       assert(*root >= mem.heap); */
  /*       assert(*root < (mem.heap+mem.heap_size)); */
  /*     } */
  /*   } */
  /*   LOG_DEBUG("\n"); */
  /*   stk = stk->parent; */
  /* } */
}

uint32_t *mem_new(uint16_t word_size)
{
  if ((mem.next_free-mem.heap+word_size) >= mem.heap_size) {
    mem_gc();
    if ((mem.next_free-mem.heap+word_size) >= mem.heap_size) {
      FATAL("Out of memory");
    }
  }
  uint32_t *r = mem.next_free;
  assert((((uint32_t)r)&3) == 0);
  mem.next_free += (word_size);
  return r;
}

