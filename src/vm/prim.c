#include "prim.h"
#include "platform.h"
#include "dict.h"
#include "ffi.h"
#include "cmd.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

enum {
  EQ = 0, 
  GT, 
  LT, 
  DIV, 
  MUL, 
  SUB, 
  ADD, 
  PRINT, 
  LIST, 
  CONS,
  FIRST, 
  REST, 
  COUNT, 
  NTH, 
  THROW, 
  LISTP,
  APPEND,
  AREF,
  STRINGLENGTH,
  STRINGAPPEND,
  NOT,
  NULLP,
  MAKEHASH,
  HASHSET,
  HASHREF,
  FFISYM,
  FFICALL,
};

#define CHECK_ARGS(n) if (n != arg_count) { common_error(ERROR_VM_PRIM_ARG_COUNT); }
#define CHECK_TYPE(v, t) if (!obj_is_type(v, t)) { common_error(ERROR_VM_PRIM_ARG_TYPE); }
#define CHECK_TYPE0(v, t) if (!(obj_is_type(v, t) || obj_is_type(v, OBJ_TYPE_NIL))) \
                               { common_error(ERROR_VM_PRIM_ARG_TYPE); }
#define BIN_OP(op) { \
  CHECK_ARGS(2); \
  a = vm_stack_pop(vm, &vm->ds); \
  b = vm_stack_pop(vm, &vm->ds); \
  CHECK_TYPE(a, OBJ_TYPE_NUM); \
  CHECK_TYPE(b, OBJ_TYPE_NUM); \
  r = NUM_NEW(NUM_VALUE(a) op NUM_VALUE(b)); \
  break; \
}

obj_t *prim_call(vm_t *vm, int prim_num, uint8_t arg_count)
{
  obj_t *a = NULL, *b = NULL, *c = NULL, *r = NULL;

  GC_ENTER4(a, b, c, r);

  switch (prim_num) {
    case EQ:
      CHECK_ARGS(2);
      r = NUM_NEW(obj_cmp(vm_stack_pop(vm, &vm->ds), vm_stack_pop(vm, &vm->ds)));
      break;

    case GT: BIN_OP(>);
    case LT: BIN_OP(<);
    case MUL: BIN_OP(*);
    case SUB: BIN_OP(-);
    case ADD: BIN_OP(+);
    
    case DIV:
      CHECK_ARGS(2);
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE(a, OBJ_TYPE_NUM);
      CHECK_TYPE(b, OBJ_TYPE_NUM);
      if (NUM_VALUE(b) == 0) {
        common_error(ERROR_VM_DIV_ZERO);
        r = NULL;
      } else {
        r = NUM_NEW(NUM_VALUE(a) / NUM_VALUE(b));
      }
      break;

    case PRINT:
      for (int i=0; i<arg_count; i++) {
        cmd_send_event(WIRE_CMD_PRINT, vm_stack_pop(vm, &vm->ds));
      }
      cmd_send_event(WIRE_CMD_PRINT, string_new_c("\n"));
      r = NULL;
      break;

    case LIST:
      r = list_new(arg_count);
      for (int i=0; i<arg_count; i++) {
        list_set_val(r, i, vm_stack_pop(vm, &vm->ds));
      }
      break;

    case CONS:
      CHECK_ARGS(2);
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      if (b == NULL) {
        r = list_new(1);
      } else if (obj_is_type(b, OBJ_TYPE_LIST)) {
        r = list_new(1 + list_size(b));
        for (int i=0; i<list_size(b); i++) {
          list_set_val(r, i+1, list_val(b, i));
        }
      } else {
        r = list_new(2);
        list_set_val(r, 1, b);
      }
      list_set_val(r, 0, a);
      break;

    case FIRST:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE(a, OBJ_TYPE_LIST);
      r = list_val(a, 0);
      break;

    case REST:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE(a, OBJ_TYPE_LIST);
      if (list_size(a) > 1) {
        r = list_new(list_size(a)-1);
        for (int i=0; i<list_size(r); i++) {
          list_set_val(r, i, list_val(a, i+1));
        }
      } else if (list_size(a) == 1) {
        r = NULL;
      } else {
        assert(0);
      }
      break;

    case NTH:
      CHECK_ARGS(2);
      a = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE0(a, OBJ_TYPE_LIST);
      b = vm_stack_pop(vm, &vm->ds);
      r = list_val(a, NUM_VALUE(b));
      break;

    case COUNT:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE0(a, OBJ_TYPE_LIST)
      r = NUM_NEW(list_size(a));
      break;

    case LISTP:
      CHECK_ARGS(1);
      r = NUM_NEW(obj_is_type(vm_stack_pop(vm, &vm->ds), OBJ_TYPE_LIST));
      break;

    case APPEND:
      CHECK_ARGS(2);
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE0(a, OBJ_TYPE_LIST)
      CHECK_TYPE0(b, OBJ_TYPE_LIST)
      r = list_append(a, b);
      break;

    case AREF:
      CHECK_ARGS(2);
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE(a, OBJ_TYPE_STRING);
      CHECK_TYPE(b, OBJ_TYPE_NUM);
      r = CHAR_NEW(string_char(a, NUM_VALUE(b)));
      break;

    case STRINGLENGTH:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE(a, OBJ_TYPE_STRING);
      r = NUM_NEW(string_size(a));
      break;

    case STRINGAPPEND:
      CHECK_ARGS(2);
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      CHECK_TYPE(a, OBJ_TYPE_STRING);
      CHECK_TYPE(b, OBJ_TYPE_STRING);
      r = string_append(a, b);
      break;

    case THROW:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      common_error1(ERROR_VM_THROW, a);
      break;

    case NULLP:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      r = NUM_NEW(a == NULL);
      break;

    case NOT:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      r = NUM_NEW(!num_value(a));
      break;

    case MAKEHASH:
      CHECK_ARGS(0);
      r = dict_new(8);
      break;

    case HASHSET:
      CHECK_ARGS(3);
      r = vm_stack_pop(vm, &vm->ds);
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      dict_set(&r, a, b);
      break;

    case HASHREF:
      CHECK_ARGS(2);
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      dict_get(a, b, &r);
      break;

    case FFISYM:
      CHECK_ARGS(1);
      a = vm_stack_pop(vm, &vm->ds);
      r = platform_ffi_sym_lookup(a);
      break;
    
    case FFICALL:
      a = vm_stack_pop(vm, &vm->ds);
      b = vm_stack_pop(vm, &vm->ds);
      c = vm_stack_pop(vm, &vm->ds);
      r = ffi_call(a, b, c);
      break;

    default:
      common_error1(ERROR_VM_BAD_PRIM, NUM_NEW(prim_num));
  }
  GC_EXIT(r);
}

