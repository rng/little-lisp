#include "wire.h"
#include "common.h"
#include "ffi.h"
#include "dict.h"
#include <stdio.h>
#include <string.h>

typedef enum {
  WIRE_TYPE_BYTEARRAY = 0,
  WIRE_TYPE_STRING,
  WIRE_TYPE_SYMBOL,
  WIRE_TYPE_INT,
  WIRE_TYPE_BOOL,
  WIRE_TYPE_NIL,
  WIRE_TYPE_OBJ,
  WIRE_TYPE_CHAR,
  WIRE_TYPE_LIST,
  WIRE_TYPE_DICT,
  WIRE_TYPE_FFI,
} wire_type_t;

typedef struct __attribute__((__packed__)) {
  uint8_t len;
  uint8_t typ;
  union {
    uint8_t buf[1];
    uint32_t u32;
    uint8_t u8;
  } data;
} wire_obj_t;

typedef struct __attribute__((__packed__)) {
  uint8_t cmd;
  wire_obj_t obj;
} cmd_t;

static size_t wire_encode_into(uint8_t *buf, size_t buf_len, obj_t *obj)
{
  assert(buf_len >= 2);
  wire_obj_t *w = (wire_obj_t*)buf;

  switch (obj_type(obj)) {
    case OBJ_TYPE_NUM:
      w->typ = WIRE_TYPE_INT;
      w->len = 4;
      w->data.u32 = NUM_VALUE(obj);
      break;

    case OBJ_TYPE_SYMBOL:
      w->typ = WIRE_TYPE_SYMBOL;
      w->len = 4;
      w->data.u32 = symbol_value(obj);
      break;

    case OBJ_TYPE_CHAR:
      w->typ = WIRE_TYPE_CHAR;
      w->len = 1;
      w->data.u8 = char_value(obj);
      break;

    case OBJ_TYPE_NIL:
      w->typ = WIRE_TYPE_NIL;
      w->len = 0;
      break;

    case OBJ_TYPE_STRING:
    {
      bytearray_t *b = (bytearray_t*)obj;
      w->typ = WIRE_TYPE_STRING;
      w->len = string_size(obj);
      memcpy(w->data.buf, b->data, b->bytesize);
      break;
    }
    
    case OBJ_TYPE_BYTEARRAY:
    {
      bytearray_t *b = (bytearray_t*)obj;
      w->typ = WIRE_TYPE_BYTEARRAY;
      w->len = bytearray_size(obj);
      memcpy(w->data.buf, b->data, b->bytesize);
      break;
    }

    case OBJ_TYPE_LIST:
    {
      w->typ = WIRE_TYPE_LIST;
      w->len = 0;
      uint8_t *cbuf = w->data.buf;
      size_t cbuf_len = buf_len - 2;
      for (int i=0; i<obj->size; i++) {
        size_t csize = wire_encode_into(cbuf, cbuf_len, list_val(obj, i));
        cbuf += csize;
        cbuf_len -= csize;
        w->len += csize;
      }
      break;
    }

    case OBJ_TYPE_DICT:
    {
      w->typ = WIRE_TYPE_DICT;
      w->len = 0;
      uint8_t *cbuf = w->data.buf;
      size_t cbuf_len = buf_len - 2;
      for (int i=0; i<obj->size; i++) {
        size_t csize = wire_encode_into(cbuf, cbuf_len, (obj_t*)obj->data[i]);
        cbuf += csize;
        cbuf_len -= csize;
        w->len += csize;
      }
      break;
    }

    case OBJ_TYPE_FFI:
      w->typ = WIRE_TYPE_FFI;
      w->len = 4;
      w->data.u32 = (uint32_t)obj->data[0];
      break;

    case OBJ_TYPE_FUNC:
      w->typ = WIRE_TYPE_OBJ;
      w->len = 4;
      w->data.u32 = (uint32_t)obj;
      break;

    default:
      FATAL("tried to encode object of type %d", obj_type(obj));
  }
  return  w->len + 2;
}

#define DECODE_CHILD(OBJ) { \
  size_t sz = wire_decode_from(cbuf, cbuf_len, OBJ); \
  cbuf += sz; \
  count += sz; \
  cbuf_len -= sz; \
}

size_t wire_decode_from(uint8_t *buf, size_t buf_len, obj_t **obj)
{
  wire_obj_t *w = (wire_obj_t*)buf;
  if ((buf_len-2) < w->len) {
    FATAL("bad decode");
  }

  size_t count = 0;
  uint8_t *cbuf = (uint8_t*)w->data.buf;
  size_t cbuf_len = w->len;
  obj_t *k = NULL;
  obj_t *v = NULL;
  GC_ENTER3(*obj, k, v);
  switch (w->typ) {
    case WIRE_TYPE_BYTEARRAY:
      *obj = bytearray_new(w->len, w->data.buf);
      break;

    case WIRE_TYPE_STRING:
      *obj = string_new(w->len, (char*)w->data.buf);
      break;
    
    case WIRE_TYPE_SYMBOL:
      *obj = symbol_new(w->data.u32);
      break;
    
    case WIRE_TYPE_INT:
      *obj = NUM_NEW(w->data.u32);
      break;
    
    case WIRE_TYPE_BOOL:
      *obj = NUM_NEW(w->data.u8);
      break;

    case WIRE_TYPE_CHAR:
      *obj = CHAR_NEW(w->data.u8);
      break;
    
    case WIRE_TYPE_NIL:
      *obj = NULL;
      break;

    case WIRE_TYPE_LIST:
      *obj = NULL;
      while (count < w->len) {
        DECODE_CHILD(&v);
        *obj = list_cons(*obj, v);
      }
      break;

    case WIRE_TYPE_DICT:
      *obj = dict_new(8);
      while (count < w->len) {
        DECODE_CHILD(&k);
        DECODE_CHILD(&v);
        dict_set(obj, k, v);
      }
      break;

    case WIRE_TYPE_FFI:
      *obj = ffi_new((void*)w->data.u32);
      break;

    case WIRE_TYPE_OBJ:
      // no way to decode generic obj references yet
      *obj = string_new_c("**obj-ref**");
      break;

    default:
      FATAL("tried to decode object of type %d", w->typ);
  }
  GC_EXIT(w->len + 2);
}

bool wire_encode(uint8_t *buf, size_t *buf_len, wire_cmd_t cmd, obj_t *obj)
{
  cmd_t *c = (cmd_t*)buf;
  c->cmd = cmd;
  *buf_len = wire_encode_into((uint8_t*)&c->obj, *buf_len-1, obj) + 1;
  return true;
}

ssize_t wire_decode(uint8_t *buf, size_t buf_len, wire_cmd_t *cmd, obj_t **obj)
{
  if (buf_len < 3) {
    return -1;
  }
  cmd_t *c = (cmd_t*)buf;
  *cmd = c->cmd;

  GC_ENTER1(*obj);
  size_t r = 1 + wire_decode_from((uint8_t*)&c->obj,buf_len-1, obj);
  GC_EXIT(r);
}
