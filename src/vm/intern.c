#include "intern.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#define MAX_INTERNS (256)
char *interns[MAX_INTERNS];
int intern_count = 0;

uint32_t intern(char *s) {
  assert(intern_count < MAX_INTERNS);
  for (int i=0;i<intern_count;i++) {
    if (strcmp(interns[i], s) == 0) {
      return i;
    }
  }
  interns[intern_count] = malloc(strlen(s)+1);
  strcpy(interns[intern_count++], s);
  return intern_count-1;
}

char *intern_name(uint32_t v) {
  if (v>=intern_count) {
    return ":<?>";
  }
  //assert(v<intern_count);
  return interns[v];
}
