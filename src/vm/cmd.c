#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "cmd.h"
#include "mem.h"

typedef struct {
  server_t srv;
} cmd_priv_t;

cmd_priv_t cmd_priv;

// remote command handlers

static void cmd_handle_run(vm_t *vm, bytearray_t *code)
{
  GC_ENTER1(code);
  vm_reset(vm);
  vm_load_code(vm, code);
  vm_set_running(vm, true);
  GC_EXIT0();
}

static void cmd_handle_stop(vm_t *vm)
{
  vm_set_running(vm, false);
}

static obj_t *cmd_handle_get_exception_param(vm_t *vm)
{
  return _exception_obj;
}

static void cmd_handle_reset(vm_t *vm)
{
  vm_init(vm);
}

static void cmd_process(void *ctx, uint8_t *buf, size_t buf_len, size_t *resp_len)
{
  vm_t *vm = (vm_t*)ctx;
  wire_cmd_t cmd;
  wire_cmd_t rcmd = WIRE_CMD_NACK;
  obj_t *value = NULL;
  obj_t *r = NULL;

  GC_ENTER2(value, r);

  ssize_t decode_len = wire_decode(buf, buf_len, &cmd, &value);
  if (decode_len != -1) {
    switch (cmd) {
      case WIRE_CMD_RUN:
        cmd_handle_run(vm, (bytearray_t*)value);
        rcmd = WIRE_CMD_CONT;
        break;

      case WIRE_CMD_STOP:
        cmd_handle_stop(vm);
        rcmd = WIRE_CMD_FAILED;
        break;

      case WIRE_CMD_RESET:
        cmd_handle_reset(vm);
        rcmd = WIRE_CMD_ACK;
        break;

      case WIRE_CMD_GET_EXCEPTION_PARAM:
        r = cmd_handle_get_exception_param(vm);
        rcmd = WIRE_CMD_ACK;

      default:
        break;
    }
  }
  wire_encode(buf, resp_len, rcmd, r);

  GC_EXIT0();
}

static void cmd_do_vm_slice(vm_t *vm)
{
  gc_stack_t *exception;
  obj_t *backtrace = NULL;
  bool done = true;
  bool ok = true;
  GC_ENTER1(backtrace);
  // run a slice of VM time (100 instructions)
  TRY(exception) {
    done = vm_exec(vm, 100);
  } else {
    EXCEPT_ENTER(exception);
    backtrace = vm_backtrace(vm, _exception_err);
    ok = false;
  }
  // if we've finished executing (successfully or exception)
  // send a response to the host (result or backtrace)
  if (done) {
    vm_set_running(vm, false);
    if (ok) {
      cmd_send_event(WIRE_CMD_STOPPED, vm_get_result(vm));
    } else {
      cmd_send_event(WIRE_CMD_FAILED, backtrace);
    }
  }
  GC_EXIT0();
}

// public

void cmd_mainloop(vm_t *vm)
{
  server_init(&cmd_priv.srv, vm, cmd_process);
  while (1) {
    if (vm_running(vm)) {
      cmd_do_vm_slice(vm);
      server_tick(&cmd_priv.srv, false);
    } else {
      server_tick(&cmd_priv.srv, true);
    }
  }
}

void cmd_send_event(wire_cmd_t event, obj_t *obj)
{
  uint8_t buf[64];
  size_t buf_len = sizeof(buf);
  wire_encode(buf, &buf_len, event, obj);
  server_send(&cmd_priv.srv, buf, buf_len);
}

