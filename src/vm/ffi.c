#include "ffi.h"
#include "common.h"
#include <string.h>

/**
 * Foreign Function Interface (lisp<->C)
 *
 * `ffi_call` takes a native pointer along with a signature string (see lib/ffi.lisp),
 * translates an arg list into native types, calls the native function, then translates
 * the native return value.
 **/

typedef void* (*ffi_func0)(void);
typedef void* (*ffi_func1)(void*);
typedef void* (*ffi_func2)(void*, void*);
typedef void* (*ffi_func3)(void*, void*, void*);
typedef void* (*ffi_func4)(void*, void*, void*, void*);

obj_t *ffi_new(void *ptr)
{
  obj_t *r = obj_new(OBJ_TYPE_FFI, 1);
  r->data[0] = (uint32_t)ptr;
  return r;
}

void *ffi_value(obj_t *f)
{
  return (void*)f->data[0];
}

void* ffi_raw_call(void *func, int nargs, void **args)
{
  void *ret = NULL;
  switch (nargs) {
    case 0: ret = ((ffi_func0)func)(); break;
    case 1: ret = ((ffi_func1)func)(args[0]); break;
    case 2: ret = ((ffi_func2)func)(args[0], args[1]); break;
    case 3: ret = ((ffi_func3)func)(args[0], args[1], args[2]); break;
    case 4: ret = ((ffi_func4)func)(args[0], args[1], args[2], args[3]); break;
    default:
      common_error(ERROR_FFI_TO_MANY_ARGS);
      break;
  }
  return ret;
}

void *ffi_to_native(char type, obj_t *v)
{
  switch (type) {
    case 'c': 
      return (void*)((uint32_t)char_value(v));
    case 's': 
      return (void*)(string_value(v));
    case 'i':
      return (void*)num_value(v);
    case 'f':
      return ffi_value(v);
    default:
      common_error(ERROR_FFI_BAD_TYPE);
      break;
  }
  return NULL;
}

obj_t *ffi_from_native(char type, void *v)
{
  obj_t *ret = NULL;
  GC_ENTER1(ret);
  switch (type) {
    case 'c': 
      ret = CHAR_NEW((char)((uint32_t)v));
      break;
    case 's': 
      ret = string_new_c((char*)v);
      break;
    case 'i':
      ret = NUM_NEW((int32_t)v);
      break;
    case 'f':
      ret = ffi_new(v);
      break;
    default:
      common_error(ERROR_FFI_BAD_TYPE);
      break;
  }
  GC_EXIT(ret);
}

obj_t *ffi_call(obj_t *func, obj_t *signature, obj_t *args)
{
  GC_ENTER3(func, signature, args);
  if ((!obj_is_type(func, OBJ_TYPE_FFI)) ||
      (!obj_is_type(signature, OBJ_TYPE_STRING)) ||
      (!obj_is_type(args, OBJ_TYPE_LIST))) {
    common_error(ERROR_OBJ_INVALID_TYPE);
  }

  if (string_size(signature) != (1+list_size(args))) {
    common_error(ERROR_FFI_BAD_SIGNATURE);
  }

  void* fargs[4];
  for (int i=0; i<list_size(args); i++) {
    char argtype = string_char(signature, i+1);
    fargs[i] = ffi_to_native(argtype, list_val(args, i));
  }

  void* ret = ffi_raw_call(ffi_value(func), list_size(args), fargs);
  char rettype = string_char(signature, 0);
  GC_EXIT(ffi_from_native(rettype, ret));
}

