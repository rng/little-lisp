; A rather basic C FFI
;
; Types:
; - i: signed 32 bit int
; - c: character
; - s: string
; - f: pointer (FFI object)
;
; # define a c function
; > (def printf (ffi-fn "printf" "is"))
;
; # use it
; > (printf "hello world\n")
; hello world
; 12

; We only have symbol lookup capabilities on the host, so do the actual lookup
; at compile time (translate it into a address to the C function)
(defmacro ffi-sym (lambda (name)
  (%ffi-lookup-sym name)))

; Look a symbol and wrap it with a signature to be called directly as a function
(defmacro ffi-fn (lambda (name signature)
  (let ((fn (%ffi-lookup-sym name)))
    `(lambda (& args)
          (ffi-call ,fn ,signature args)))))

