(def *lexpos* 0)
(def *lexstr* nil)

(defun lsym (s) (cons s 'sym))

(defun ln (n) (set! *lexpos* (+ *lexpos* n)))

(defun isalpha (c) false)

(defun lex ()
  (let ((c (aref *lexstr* *lexpos*)))
;    (print c)
    (cond
      (= c #\() (do (ln 1) (lsym "("))
      (= c #\)) (do (ln 1) (lsym ")"))
      (isalpha c) (lex-sym)
      true (throw "unhandled character" c))))

(defun lexall ()
  (set! x (lex))
  ;(print *lexpos*)
  (print x)
  (if (not (null x))
    (lexall)))

(set! *lexstr* "(do (+ 1 2) 'foo)")
(lexall)
