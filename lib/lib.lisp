; Bytecode doesn't let us refer to primitives as first-class objects so wrap
; them here.

(def first (lambda (x) (%first x)))
(def rest (lambda (x) (%rest x)))
(def cons (lambda (a b) (%cons a b)))
(def + (lambda (a b) (%+ a b)))
(def - (lambda (a b) (%- a b)))
(def * (lambda (a b) (%* a b)))
(def / (lambda (a b) (%/ a b)))

; Basics we'll need for initial macros

; list accessors
(def cadr (lambda (x) (nth x 1)))
(def caddr (lambda (x) (nth x 2)))
(def cddr (lambda (x) (rest (rest x))))
(def second cadr)
(def third caddr)

; map
(def map (lambda (func lst)
           (if (> (count lst) 0)
             (cons (func (first lst))
                   (map func (rest lst)))
             nil)))

; cond
(defmacro cond (lambda (& xs) 
  (if (> (count xs) 0) 
    (list 'if (first xs)
          (if (> (count xs) 1)
            (cadr xs) 
            (throw "odd number of forms to cond"))
          (if (= (count xs) 1)
            (throw "odd number of forms to cond")
            (cons 'cond (rest (rest xs))))))))

; Boolean and & or
(defmacro and (lambda (a b)
  (list 'if a b false)))

(defmacro or (lambda (a b)
  (list 'if a a b)))

; quasiquote
(defmacro quasiquote (lambda (x)
  (if (and (list? x) (> (count x) 0))
    (cond 
      (= 'unquote (first x)) (cadr x)
      (= 'quasiquote (first x)) (quasiquote (quasiquote (cadr x)))
      (list? (first x))
        (if (= 'splice-unquote (first (first x)))
          (list 'append (cadr (first x)) (quasiquote (rest x)))
          (list 'cons (quasiquote (first x)) (quasiquote (rest x))))
      true (list 'cons (quasiquote (first x)) (quasiquote (rest x))))
    (list 'quote x))))

; With quasiqoute we can write a few useful macros cleanly

; let
(defmacro let (lambda (defs & body)
  `((lambda ,(map first defs) ,@body)
    ,@(map cadr defs))))

; letrec
(defmacro letrec (lambda (defs & body)
  (let ((names (map (lambda (x) (list (first x) nil)) defs))
        (sets (map (lambda (x) (list 'set! (first x) (cadr x))) defs)))
    `(let ,names ,@sets ,@body))))

; defun
(defmacro defun (lambda (name args & body)
  `(def ,name (lambda ,args ,@body))))

; assert
(defmacro assert (lambda (value & msg)
  (if (> (count msg) 0)
    `(if (not ,value) (throw ,(first msg)))
    `(if (not ,value) (throw "assertion failed")))))

; do loop
(defmacro do (lambda (bindings test-and-result & body)
  (let ((variables (map first bindings))
        (inits (map second bindings))
        (steps (map (lambda (clause)
                      (if (null? (cddr clause))
                        (first clause)   
                        (third clause)))
                    bindings))
        (test (first test-and-result))
        (result (rest test-and-result)))
      `(letrec ((_loop
                  (lambda ,variables
                    (if ,test
                      (begin ,@result)
                      (begin 
                        ,@body
                        (_loop ,@steps))))))
         (_loop ,@inits)))))

; List stuff

; fold
(defun fold (fn knil lst)
  (letrec ((loop (lambda (lis r)
    (if (null? lis)
      r
      (loop (rest lis) (fn (first lis) r))))))
    (loop lst knil)))

; reverse
(defun reverse (lst)
  (fold cons '() lst))

; iota
(defun iota (num & maybe-start)
  (let ((start 0))
    (if (not (null? maybe-start))
      (set! start (first maybe-start)))

    (letrec ((loop (lambda (n r)
      (if (= n num)
        (reverse r)
        (loop (+ 1 n) (cons (+ start n) r))))))
      (loop 0 '()))))
