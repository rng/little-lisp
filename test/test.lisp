(defun fact (n)
  (if (> n 0)
    (* n (fact (- n 1)))
    1))

(assert (= (fact 5) 120))

(defun ifact (n)
  (letrec ((f (lambda (i a)
    (if (= i 0)
      a
      (f (- i 1) (* a i))))))
    (f n 1)))

(assert (= (ifact 10) 3628800))

; arg order
(defun sub (a b) (- a b))
(assert (= (sub 4 1) 3))

; lambda
(assert (= ((lambda (x) (+ x 1)) 9) 10))

; closure
(defun counter (n)
               (lambda (incr) 
                 (begin
                   (set! n (+ n incr)) 
                   n)))

(def foo (counter 5))
(assert (= (foo 1) 6))
(assert (= (foo 2) 8))
(assert (= (foo 4) 12))

; varargs
(defun _sum (lst)
  (if (> (count lst) 0)
    (+ (first lst) (_sum (rest lst)))
    0))

(defun sum (& args) (_sum args))

(assert (= (sum 1 2 3 4 5 6) 21))

; macros
(def n 0)
(assert (= "zero"
  (cond 
    (> n 0) "positive"
    (< n 0) "negative"
    true "zero")))

(defun plus1 (n) (+ n 1))
(defmacro next (lambda (x) (plus1 x)))
(assert (= (next 2) 3))

; fail
(defun bar (x) (/ 1 x))
(defun baz (y) (+ 1 (bar (- y 2))))
(defun baq (z) (+ 2 (baz (+ z 2))))

; map
(defun times2 (v) (* v 2))
(assert (= (map times2 (list 1 2 3 4)) '(2 4 6 8)))

; quasiquote
(def n 1)
(assert (= `1 1))
(assert (= `(bar 1) (list `bar 1)))
(assert (= `(bar ,(+ n 2)) `(bar 3)))
(assert (= `(bar ,@(list 1 2)) `(bar 1 2)))

; let
(assert (= (let ((foo 1)) (+ foo 1)) 2))

(defun l (a)
  (let ((a (+ a 4))
        (b (+ a 2)))
    (assert (= a 8))
    (assert (= b 6))
    (let ((b (- b 4)))
      (assert (= b 2))))
  (assert (= a 4)))

(l 4)

(defun looper (x)
  (if (> x 0)
    (begin
      ((lambda (y) (+ y 2)) x)
      (looper (- x 1)))
    0))

(looper 6)
(looper 6)
(looper 6)
(looper 6)

(print "passed")
(exit)
