VMOBJS = src/vm/mem.o \
				 src/vm/obj.o \
				 src/vm/dict.o \
				 src/vm/prim.o \
				 src/vm/wire.o \
				 src/vm/cmd.o \
				 src/vm/common.o \
				 src/vm/intern.o \
				 src/vm/ffi.o \
				 src/vm/vm.o \

COMPOBJS = src/comp/lex.o \
			     src/comp/comp.o \
			     src/comp/macro.o \
			     src/comp/parse.o \
			     src/comp/codegen.o \
			     src/comp/env.o \

REPLOBJS = src/llisp.o \
					 src/lvm.o \
					 src/backtrace.o \
					 src/remote.o \

PLATOBJS = src/plat/host.o \
					 src/plat/socket.o \

LVMOBJS = src/lvm.o \
					src/lvmmain.o \

CFLAGS = -std=gnu11 -Wall -m32 -g -O0 -I inc
LDFLAGS = -lm -lreadline -ldl

all: llisp lvm

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

llisp: $(REPLOBJS) $(VMOBJS) $(PLATOBJS) $(COMPOBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

lvm: $(VMOBJS) $(PLATOBJS) $(LVMOBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

wc:
	find . -name "*.[ch]" | xargs wc -l | sort -n

.PHONY: test
test: llisp test/test.lisp
	./llisp test/test.lisp

.PHONY: clean
clean:
	rm -f $(VMOBJS) $(COMPOBJS) $(PLATOBJS) $(REPLOBJS) $(LVMOBJS) llisp lvm

