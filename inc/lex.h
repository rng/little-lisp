#pragma once

#include <stdint.h>

typedef enum {
  TOKEN_EOF = 0,
  TOKEN_INT,
  TOKEN_SYM,
  TOKEN_STR,
} token_type_t;

typedef struct {
  token_type_t t;
  union {
    int32_t i;
    char *s;
  };
} token_t;

typedef struct {
  uint32_t pos;
  uint32_t line;
  char *linestart;
  char *buf;
  char *cur;
  char *strbuf;
  token_t tok;
} lex_t;

void lex_init(lex_t *lex, char *s);
token_t *lex_next(lex_t *lex);
