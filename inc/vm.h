#pragma once

#include <stdint.h>
#include "obj.h"
#include "common.h"

#define VM_DS_SIZE (16)
#define VM_CS_SIZE (128)

typedef struct vm_stack_t {
  uint16_t size;
  uint16_t top;
  obj_t *buf;
} vm_stack_t;

typedef struct vm_t
{
  bytearray_t *code;
  obj_t *env;
  obj_t *frame;
  vm_stack_t ds;
  vm_stack_t cs;

  uint32_t codesize;
  uint32_t pc;
  bool running;
} vm_t;

void vm_init(vm_t *vm);
void vm_reset(vm_t *vm);
obj_t *vm_run(vm_t *vm, bytearray_t *code);
obj_t *vm_backtrace(vm_t *vm, common_error_t err);
obj_t *vm_get_result(vm_t *vm);
bool vm_exec(vm_t *vm, int max_insts);
void vm_load_code(vm_t *vm, bytearray_t *code);
void vm_set_running(vm_t *vm, bool running);
bool vm_running(vm_t *vm);

void vm_stack_push(vm_t *vm, vm_stack_t *s, obj_t *v);
obj_t *vm_stack_pop(vm_t *vm, vm_stack_t *s);
