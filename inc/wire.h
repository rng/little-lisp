#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "obj.h"

typedef enum {
  WIRE_CMD_NONE = 0,
  // client->server
  WIRE_CMD_RUN,
  WIRE_CMD_STOP,
  WIRE_CMD_RESET,
  WIRE_CMD_GET_EXCEPTION_PARAM,
  // server->client
  WIRE_CMD_ACK,
  WIRE_CMD_NACK,
  WIRE_CMD_CONT,
  WIRE_CMD_ERR,
  WIRE_CMD_PRINT,
  WIRE_CMD_STOPPED,
  WIRE_CMD_FAILED,
} wire_cmd_t;

bool wire_encode(uint8_t *buf, size_t *buf_len, wire_cmd_t cmd, obj_t *obj);
ssize_t wire_decode(uint8_t *buf, size_t buf_len, wire_cmd_t *cmd, obj_t **obj);
