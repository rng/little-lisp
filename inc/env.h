#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "obj.h"

typedef struct env_t {
  struct env_t *parent;
  obj_t **vars;
  uint32_t num_vars;
} env_t;

env_t *env_new();
void env_free(env_t *env);
bool env_get(env_t *env, obj_t *sym, int *d, int *n);
env_t *env_extend(env_t *parent, obj_t *args, int *argcount, bool *vararg);
