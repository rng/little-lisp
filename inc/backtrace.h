#pragma once

#include "comp.h"

void backtrace(const char *mode, obj_t *trace, obj_t *exc_obj, comp_state_t *compstate);

