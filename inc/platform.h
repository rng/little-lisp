#pragma once

#include "obj.h"

obj_t *platform_ffi_sym_lookup(obj_t* name);
void platform_fatal(char *msg, ...) __attribute__ ((noreturn)); 
