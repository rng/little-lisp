#pragma once

typedef enum {
  VM_OP_GET = 0,
  VM_OP_SET, 
  VM_OP_AGET,
  VM_OP_AGET0, 
  VM_OP_ASET, 
  VM_OP_IMM, 
  VM_OP_LIT, 
  VM_OP_JMP, 
  VM_OP_JZ, 
  VM_OP_PRIM, 
  VM_OP_CALL, 
  VM_OP_TCALL, 
  VM_OP_FN, 
  VM_OP_FNV, 
  VM_OP_SPCL
} vm_op_t;

typedef enum {
  VM_SPCL_RET = 0, 
  VM_SPCL_NIL, 
  VM_SPCL_TRUE, 
  VM_SPCL_FALSE,
  VM_SPCL_POP,
  VM_SPCL_LITS,
} vm_spcl_t;

typedef enum {
  VM_LIT_NONE = 0,
  VM_LIT_STRING,
  VM_LIT_SYMBOL,
  VM_LIT_INT16,
  VM_LIT_INT32,
  VM_LIT_LIST,
  VM_LIT_CHAR,
  VM_LIT_FFI,
} vm_literal_t;

