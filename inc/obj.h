#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "mem.h"

#define NUM_NEW(v) ((obj_t*)(((v)<<2)|OBJ_TYPE_NUM))
#define NUM_VALUE(v) num_value(v)
#define CHAR_NEW(v) ((obj_t*)(((v)<<2)|OBJ_TYPE_CHAR))

#define OBJ_CHECK_BOUNDS(index, max) { \
  if ((index < 0) || (index >= max)) { \
    common_error(ERROR_OBJ_OUT_OF_BOUNDS); \
  } \
}

typedef struct __attribute__((__packed__)) {
  uint16_t typ;
  uint16_t size;
  uint32_t bytesize;
  uint8_t data[];
} bytearray_t;

typedef struct __attribute__((__packed__)) {
  uint32_t hdr;
  uint8_t argcount;
  uint8_t varargs;
  uint16_t fnid;
  obj_t *frame;
  obj_t *code;
} func_t;

typedef struct __attribute__((__packed__)) {
  uint16_t typ;
  uint16_t size;
  obj_t *parent;
  obj_t *vals[];
} frame_t;

obj_t *obj_new(obj_type_t typ, uint16_t word_size);

// common
bool obj_is_type(obj_t *o, obj_type_t typ);
obj_type_t obj_type(obj_t *o);
obj_t *obj_to_string(obj_t *o);
obj_t *obj_to_string_raw(obj_t *o);
bool obj_cmp(obj_t *a, obj_t *b);
uint32_t obj_hash(obj_t *o);

// function
obj_t *func_new(uint16_t argcount,
                bool varargs,
                uint16_t fnid,
                obj_t *frame,
                obj_t *code);
uint16_t func_argcount(obj_t *o);
obj_t *func_frame(obj_t *o);
uint16_t func_fnid(obj_t *o);
bool func_varargs(obj_t *o);
obj_t *func_code(obj_t *o);

// frame
obj_t *frame_new(obj_t *parent, uint32_t sz);
obj_t *frame_val(obj_t *o, uint32_t idx);
void frame_set_val(obj_t *o, uint32_t idx, obj_t *v);
obj_t *frame_deep_val(obj_t *o, uint32_t depth, uint32_t idx);
void frame_deep_set_val(obj_t *o, uint32_t depth, uint32_t idx, obj_t *v);

// list
obj_t *list_new(uint16_t size);
uint32_t list_size(obj_t *o);
obj_t *list_val(obj_t *l, uint32_t idx);
void list_set_val(obj_t *l, uint32_t idx, obj_t *v);
obj_t *list_append(obj_t *l1, obj_t *l2);
obj_t *list_cons(obj_t *l1, obj_t *v);

// bytearray
obj_t *bytearray_new(uint16_t size, uint8_t *data);
uint32_t bytearray_size(obj_t *o);
uint32_t bytearray_val(obj_t *o, int idx);

// string
obj_t *string_new(uint16_t size, char *data);
obj_t *string_new_c(char *data);
char *string_value(obj_t *o);
obj_t *string_append(obj_t *o, obj_t *b);
uint32_t string_size(obj_t *o);
char string_char(obj_t *o, uint32_t idx);

// symbol
obj_t *symbol_new(uint32_t value);
uint32_t symbol_value(obj_t *o);

// number
int32_t num_value(obj_t *o);

// char
char char_value(obj_t *o);

