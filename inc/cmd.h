#pragma once

#include <stdint.h>
#include "wire.h"
#include "socket.h"
#include "vm.h"

void cmd_mainloop(vm_t *vm);
void cmd_send_event(wire_cmd_t event, obj_t *obj);
