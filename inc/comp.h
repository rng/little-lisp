#pragma once

#include "obj.h"
#include <stdbool.h>

typedef struct {
  uint16_t func_count;
  obj_t *debugmap;
} comp_state_t;

obj_t *comp_compile(comp_state_t *state, obj_t *expr, void *macros, bool *isdef);
