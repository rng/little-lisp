#pragma once

#include "obj.h"

obj_t *ffi_new(void *ptr);
void *ffi_value(obj_t *f);
obj_t *ffi_call(obj_t *func, obj_t *signature, obj_t *args);
