#pragma once

#include <stdint.h>

uint32_t intern(char *s);
char *intern_name(uint32_t v);
