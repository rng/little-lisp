#pragma once

typedef union {
  void*   vp;
  int32_t i32;
  int16_t i16;
} literal_t;
