#pragma once

#include "obj.h"
#include "platform.h"
#include <setjmp.h>

#define STR2(x) #x
#define STR(x) STR2(x)
#define FATAL(...) platform_fatal(__FILE__":"STR(__LINE__)" Fatal error: " __VA_ARGS__)

#if DEBUG
#include <stdio.h>
#define LOG_DEBUG(...) printf(__VA_ARGS__);
#else
#define LOG_DEBUG(...)
#endif

typedef enum {
  ERROR_VM_NONE=0,
  ERROR_VM_UNKNOWN_GLOBAL,
  ERROR_VM_PRIM_ARG_COUNT,
  ERROR_VM_PRIM_ARG_TYPE,
  ERROR_VM_BAD_PRIM,
  ERROR_VM_BAD_OP,
  ERROR_VM_BAD_FUNC,
  ERROR_VM_FUNC_ARG_COUNT,
  ERROR_VM_DIV_ZERO,
  ERROR_VM_THROW,
  ERROR_VM_STACK_OVERFLOW,
  ERROR_VM_STACK_UNDERFLOW,
  ERROR_COMP_PRIM_REF,
  ERROR_PARSE_BAD_TOKEN,
  ERROR_PARSE_UNEXPECTED,
  ERROR_PARSE_UNEXPECTED_LIST,
  ERROR_OBJ_INVALID_TYPE,
  ERROR_OBJ_OUT_OF_BOUNDS,
  ERROR_LEX_BAD_ESCAPE,
  ERROR_LEX_BAD_CHAR,
  ERROR_FFI_TO_MANY_ARGS,
  ERROR_FFI_BAD_SIGNATURE,
  ERROR_FFI_BAD_TYPE,
} common_error_t;

#define TRY(stk) \
  stk = _gc_roots; \
  int _exception_err = setjmp(_exception_jb); \
  if (_exception_err == 0)

#define EXCEPT_ENTER(stk) { _gc_roots = stk; }

extern jmp_buf _exception_jb;
extern obj_t *_exception_obj;

const char *common_error_str(common_error_t err);
void common_error(common_error_t err);
void common_error1(common_error_t err, obj_t *obj);
