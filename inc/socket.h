#pragma once

typedef void (*server_process_cb_t)(void *ctx,
                                    uint8_t *buf,
                                    size_t buf_len,
                                    size_t *resp_len);

typedef struct {
  int sock;
  int client_sock;
  int max_sock;
  void *context;
  fd_set active_fd_set;
  server_process_cb_t callback;
} server_t;

void server_init(server_t *srv, void *context, server_process_cb_t callback);
void server_send(server_t *srv, uint8_t *buf, size_t buf_len);
void server_tick(server_t *srv, bool blocking);

int client_init(void);
void client_shutdown(int client);
void client_send(int client, uint8_t *buf, size_t buf_len);
int client_recv(int client, uint8_t *buf, size_t buf_len);
