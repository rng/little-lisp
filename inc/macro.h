#pragma once

#include <stdbool.h>
#include "comp.h"
#include "obj.h"
#include "vm.h"

#define MAX_MACROS (32)

typedef struct {
  vm_t vm;
  obj_t *macros[MAX_MACROS];
  uint32_t num_macros;
  comp_state_t *compstate;
} macros_t;

void macro_init(macros_t *m, comp_state_t *compstate);
bool is_macro(macros_t *m, obj_t *sym);
void set_macro(macros_t *m, obj_t *expr);
obj_t *macro_eval(macros_t *m, obj_t *code);
obj_t *macro_expand(macros_t *m, obj_t *expr);
