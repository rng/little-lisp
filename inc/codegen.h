#pragma once

#include "mem.h"
#include "ops.h"

typedef struct {
  uint8_t *code;
  uint32_t code_len;
  uint32_t code_max;
} cg_t;

void cg_new(cg_t *cg);
void cg_byte(cg_t *cg, uint8_t b);
void cg_bytes(cg_t *cg, uint8_t *b, size_t len);
void cg_inst0(cg_t *cg, vm_op_t op, uint8_t n);
void cg_insts(cg_t *cg, vm_op_t op, uint8_t l);
void cg_instp(cg_t *cg, vm_op_t op, uint8_t d, uint8_t n);
void cg_insti(cg_t *cg, vm_op_t op, int32_t v);
int cg_instj(cg_t *cg, vm_op_t op, uint8_t arg);
void cg_patch(cg_t *cg, int pos);
void cg_funcid(cg_t *cg, int fnid);

