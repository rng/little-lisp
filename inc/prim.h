#pragma once

#include <stdint.h>
#include "mem.h"
#include "vm.h"

obj_t *prim_call(vm_t *vm, int prim_num, uint8_t arg_count);
