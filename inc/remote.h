#pragma once

#include "obj.h"

bool remote_run(bytearray_t *code, obj_t **ret);
void remote_stop(void);
obj_t *remote_get_exception();
void remote_reset(void);
void remote_break(void);
