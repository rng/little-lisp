#pragma once

#include "obj.h"

obj_t *dict_new(uint32_t size);
void dict_set(obj_t **d, obj_t *k, obj_t *v);
bool dict_get(obj_t *d, obj_t *k, obj_t **v);

