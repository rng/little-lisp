#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

typedef enum {
  OBJ_TYPE_NIL = 0,
  OBJ_TYPE_NUM,
  OBJ_TYPE_SYMBOL,
  OBJ_TYPE_CHAR,

  OBJ_TYPE_FORWARD,

  OBJ_TYPE_FUNC,
  OBJ_TYPE_FRAME,
  OBJ_TYPE_LIST,
  OBJ_TYPE_STRING,
  OBJ_TYPE_BYTEARRAY,
  OBJ_TYPE_DICT,
  OBJ_TYPE_FFI,

  OBJ_TYPE_MAX,
} obj_type_t;

typedef struct __attribute__((__packed__)) {
  uint16_t typ;
  uint16_t size;
  uint32_t data[];
} obj_t;

// keep track of GC roots with a separate root stack created on (C) function entry

typedef struct gc_stack_t {
  struct gc_stack_t *parent; // null at the top of the stack
  const char *name;          // function name for debugging
  obj_t ***roots;            // actual root objects
} gc_stack_t;

extern gc_stack_t *_gc_roots;

// create a new stack entry with N roots
#define _GC_ENTER(n, ...) \
obj_t **_stk_data[n+1] = {__VA_ARGS__, NULL}; \
gc_stack_t _stk_roots = {.parent = _gc_roots, .roots=_stk_data, .name=__FUNCTION__}; \
_gc_roots = &_stk_roots;

#define GC_ENTER1(a) _GC_ENTER(1, (obj_t**)&a)
#define GC_ENTER2(a,b) _GC_ENTER(2, (obj_t**)&a, (obj_t**)&b)
#define GC_ENTER3(a,b,c) _GC_ENTER(3, (obj_t**)&a, (obj_t**)&b, (obj_t**)&c)
#define GC_ENTER4(a,b,c,d) _GC_ENTER(4, (obj_t**)&a, (obj_t**)&b, (obj_t**)&c, \
                                        (obj_t**)&d)
#define GC_ENTER5(a,b,c,d,e) _GC_ENTER(5, (obj_t**)&a, (obj_t**)&b, (obj_t**)&c, \
                                          (obj_t**)&d, (obj_t**)&e)

// leave the current function, popping a root stack entry
#define GC_EXIT0() { assert(_gc_roots); _gc_roots = _gc_roots->parent; }
#define GC_EXIT(...) { GC_EXIT0(); return(__VA_ARGS__); }

//

void mem_init(uint32_t *heaps, size_t heap_word_size);
uint32_t *mem_new(uint16_t word_size);
void mem_check_obj(obj_t *o);
